---
title: "Cambridge Residences"
author: "Kent Johnson"
output:
  html_vignette:
    css: ~/Google Drive/vignette.css
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE}
library(tidyverse)
library(leaflet)
library(leaflet.extras)
library(scales)
library(sf)
knitr::opts_chunk$set(echo=FALSE,fig.width=10, fig.height=8, comment=NA, warning=FALSE, message=FALSE)
```

```{r cache=FALSE}
sq_ft_per_acre = 43560

bg_summary = st_read('ResidenceSummaryByBlockGroup.gpkg', quiet=TRUE) %>% 
  mutate(assess_per_acre=mean_assess*num_units/(Shape_Area/sq_ft_per_acre))

```

This map summarizes the number, density and assessed value of Cambridge residential
properties. Values are summarized by census block group.
<br>

```{r}
bg_names = c(
  pct_exempt_units='Percent exempt units', # Keep this as the first entry
  pct_exempt='Percent exempt parcels',
  num_units='Total dwelling units',
  num_parcels='Total residential parcels',
  num_exempt='Total exempt',
  resid_per_acre='Units per acre',
  mean_assess='Mean assessed value',
  mean_resid='Mean exempt value',
  assess_per_acre='Assessed value per acre'
)

add_layer = function(map, column, labels, trans=identity) {
  d = trans(bg_summary[[column]])
  
  # mean_assess has one huge value that throws the palette off
  # if (column=='mean_assess')
  #   d = pmin(d, max(d[d<20000000], na.rm=TRUE))
  pal = colorNumeric('RdYlGn', d)
  group_name = unname(bg_names[column])
  addPolygons(map, data=bg_summary, color='black', group=group_name,
              label=labels,
                    fillColor=pal(d), fillOpacity=0.6,
              weight=1, dashArray=1, opacity=0.7)
}

map = leaflet(options=leafletOptions(maxZoom=20)) %>% 
  addProviderTiles('CartoDB.Positron') %>% 
  setView(-71.115, 42.3769824, zoom = 13)

map = map %>%
  add_layer("num_units", comma(bg_summary$num_units), trans=log1p) %>% 
  add_layer("num_parcels", comma(bg_summary$num_parcels), trans=log1p) %>% 
  add_layer("num_exempt", comma(bg_summary$num_exempt)) %>% 
  add_layer("mean_assess", dollar(round(bg_summary$mean_assess, 0)), trans=log1p) %>% 
  add_layer("mean_resid", dollar(round(bg_summary$mean_resid, 0))) %>% 
  add_layer("pct_exempt", percent(bg_summary$pct_exempt, accuracy=2)) %>% 
  add_layer("pct_exempt_units", percent(bg_summary$pct_exempt_units, accuracy=2)) %>% 
  add_layer("resid_per_acre", as.character(round(bg_summary$resid_per_acre, 1)), trans=log1p) %>% 
  add_layer("assess_per_acre", dollar(round(bg_summary$assess_per_acre, 0))) %>% 
  addLayersControl(baseGroups=unname(bg_names), 
                   options=layersControlOptions(collapsed=FALSE)) 

for (group in unname(tail(bg_names, -1)))
  map = map %>% hideGroup(group)

map %>% showGroup(unname(bg_names[1]))
```

#### Notes

- *Parcels* are assessed properties. For example single-family homes, 
three-family buildings, 
apartment buildings and condominiums are all single parcels.
- *Units* are individual dwelling units. For multi-family homes and 
apartment buildings there are multiple units in a single parcel.
- The residential exemption applies to parcels, not units. An apartment building
  may receive the residential exemption if the owner lives in one unit.

  
- **Percent exempt units** is the percent of dwelling units receiving the 
  exemption (Total exempt / Total dwelling units).
- **Percent exempt parcels** is the percent of parcels receiving the exemption
  (Total exempt / Total residential parcels).
- **Total dwelling units** is the number of dwelling units in the
  census block group.
- **Total residential parcels** is the number of residential parcels in the
  census block group.
- **Total exempt** is the number of residential properties receiving the
  residential exemption.
- **Units per acre** is the density of dwelling units.
- **Mean assessed value** is the average assessed value of all residential 
  parcels in the block group.
- **Mean exempt value** is the average assessed value of all exempt 
  residential parcels in the block group.
- **Assessed value per acre** is total assessed value divided by
  the land area in acres.



#### Sources

- All data is from the [Cambridge Open Data](https://data.cambridgema.gov/)
  portal and [Cambridge GIS](http://gis.cambridgema.gov).
- Assessment data from Cambridge Property Database FY16-FY18 (no longer available).
- Property location from Cambridge Master Address list.

<small>Copyright `r format(Sys.Date(), '%Y')` Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle;display:inline" 
  src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" target="_blank"/></a>
  <span style="float:right;font-style: italic;">`r Sys.Date()`</span></small>
