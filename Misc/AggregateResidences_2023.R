# Join assessor's data with master address list and census block groups
# to summarize by block group and zoning
### Updated for 2023 data

library(tidyverse)
library(leaflet)
library(sf)

source(here::here('Cambridge Open Data/CODutil.R'))
# assess = read_csv(
#     here::here('Cambridge Open Data/Assess/Cambridge_Property_Database_FY2016-FY2023.csv')) |> 
#   filter(YearOfAssessment==2023)
 
# These are classified as residential but they are not dwelling units
non_resid = c("RES-PDV-LAND", "RES-UDV-LAND", "RES-DEV-LAND", 
              "RES LND-IMP UNDEV", "RES-UDV-PARK LND", "RES-LAND-IMP", 
              "RES LND-IMP PT DEV", "RES-COV-PKG", "CONDO-BLDG"
)

# resid = assess %>% 
#   filter(
#     startsWith(as.character(StateClassCode), '1'),
#     !PropertyClass %in% non_resid) %>% 
#   mutate(units = if_else(!is.na(Interior_NumUnits) & Interior_NumUnits>0,
#                         Interior_NumUnits, 1L))

# How many units did we assign to the default?
# with(resid, sum(is.na(Interior_NumUnits) | Interior_NumUnits>0))
# 23657 Almost all... :-(
# But the multi-unit properties do seem to be fairly well populated

# Join with master address list to get location
master_address = st_read(here::here('Cambridge Open Data/Shapefiles/ADDRESS_AddressPoints.shp'), stringsAsFactors=FALSE)

# resid_join = resid %>% 
#   left_join(master_address %>% filter(!duplicated(Full_Addr)), by=c(Address='Full_Addr')) %>% 
#   st_as_sf()

### FIX THIS
# There are a lot of addresses in the assessor data that don't match 
# the master address list :-(
# sum(st_is_empty(resid_join$geometry)) #5499

# Try an alternate file
assess = readxl::read_xlsx(here::here('Cambridge Open Data/Assess/FY2022_probDB.xlsx'))
resid = assess %>% 
  filter(
    startsWith(as.character(stateclasscode), '1'),
    !propertyclass %in% non_resid) %>% 
  mutate(units = if_else(!is.na(interior_numunits) & interior_numunits>0,
                         interior_numunits, 1L))

# Still missing some
sum(is.na(resid$latitude)) # 177

# Can we get them from the master address list by gisid / ml ?
sum((resid |> filter(is.na(latitude)) |> pull(gisid)) %in% master_address$ml)
# 146 not bad

resid1 = resid |> 
  filter(!is.na(longitude)) |> 
  st_as_sf(coords=c('longitude', 'latitude'), crs=4326)

resid2 = resid |> 
  filter(is.na(longitude)) |> 
  select(-longitude, -latitude) |> 
  inner_join(master_address |> select(ml) |> st_transform(4326), 
             by=c(gisid='ml'), multiple='first')
resid_join = bind_rows(resid1, resid2) # Good enough

# Now aggregate by census block group
bg = st_read(here::here('Cambridge Open Data/Shapefiles/DEMOGRAPHICS_BlockGroups2020.gdb.zip'))

bg_join = st_join(resid_join, bg |> st_transform(st_crs(resid_join)),
                  join=st_nearest_feature)
sum(is.na(bg_join$GEOID20)) # 0

sq_ft_per_acre = 43560

bg_summary = bg_join %>% 
  group_by(GEOID20, Shape_Area) %>% 
  summarize(num_parcels=sum(!is.na(pid)),
            num_units=sum(units, na.rm=TRUE),
            num_exempt=sum(residentialexemption=='True', na.rm=TRUE),
            mean_assess=mean(assessedvalue, na.rm=TRUE),
            mean_resid = mean(assessedvalue[residentialexemption=='True'], 
                              na.rm=TRUE),
            pct_exempt=num_exempt/num_parcels,
            pct_exempt_units=num_exempt/num_units) %>% 
  mutate(resid_per_acre=num_parcels/(Shape_Area/sq_ft_per_acre)) %>% 
  ungroup %>% 
  st_transform(crs=4326)

st_write(bg_summary, 'ResidenceSummaryByBlockGroup_2023.gpkg', delete_dsn=TRUE)

# Now aggregate by census block
blk = st_read(here::here('Cambridge Open Data/Shapefiles/DEMOGRAPHICS_Blocks2020.gdb.zip'))

blk_join = st_join(resid_join, blk |> st_transform(st_crs(resid_join)),
                  join=st_nearest_feature)
sum(is.na(blk_join$GEOID20)) # 0

blk_summary = blk_join %>% 
  group_by(GEOID20, Shape_Area) %>% 
  summarize(num_parcels=sum(!is.na(pid)),
            num_units=sum(units, na.rm=TRUE),
            num_exempt=sum(residentialexemption=='True', na.rm=TRUE),
            mean_assess=mean(assessedvalue, na.rm=TRUE),
            mean_resid = mean(assessedvalue[residentialexemption=='True'], 
                              na.rm=TRUE),
            pct_exempt=num_exempt/num_parcels,
            pct_exempt_units=num_exempt/num_units) %>% 
  mutate(resid_per_acre=num_parcels/(Shape_Area/sq_ft_per_acre)) %>% 
  ungroup %>% 
  st_transform(crs=4326)

st_write(blk_summary, 'ResidenceSummaryByBlock_2023.gpkg', delete_dsn=TRUE)

# Now by zoning
zoning = st_read(here::here('Cambridge Open Data/Shapefiles/CDD_ZoningDistricts.gdb.zip'))
zoning = zoning |> 
  st_cast('MULTIPOLYGON')
#|> 
#  group_by(ZONE_TYPE) |> 
#  summarize()
zoning$Shape_Area = st_area(zoning)
zoning$ID = 1:nrow(zoning)

zoning_join = st_join(resid_join, 
                      zoning |> st_transform(st_crs(resid_join)),
                   join=st_nearest_feature)
sum(is.na(zoning_join$ZONE_TYPE)) # 0

zoning_summary = zoning_join %>% 
  group_by(ZONE_TYPE, ID) %>% 
  summarize(num_parcels=sum(!is.na(pid)),
            num_units=sum(units, na.rm=TRUE),
            num_exempt=sum(residentialexemption=='True', na.rm=TRUE),
            mean_assess=mean(assessedvalue, na.rm=TRUE),
            mean_resid = mean(assessedvalue[residentialexemption=='True'], 
                              na.rm=TRUE),
            pct_exempt=num_exempt/num_parcels,
            pct_exempt_units=num_exempt/num_units,
            Shape_Area=sum(Shape_Area),
            resid_per_acre=num_parcels/(Shape_Area/sq_ft_per_acre)) %>% 
  ungroup %>% 
  st_transform(crs=4326)

# The summary gives us point geometry, we want the polygons of zoning
zoning_summary = zoning_summary |> 
  st_drop_geometry() |> 
  left_join(zoning |> select(ID)) |> 
  st_as_sf()

mapview(zoning_summary, label=zoning_summary$ZONE_TYPE, zcol='num_parcels')
st_write(zoning_summary, 'ResidenceSummaryByZoning_2023.gpkg', delete_dsn=TRUE)

# Table for Doug
library(gt)
zoning_table_data = zoning_summary |> 
  st_drop_geometry() |> 
  select(Zone=ZONE_TYPE, num_parcels, num_units) |> 
  summarize(.by=Zone, 
            num_parcels=sum(num_parcels), 
            num_units=sum(num_units)) |> 
  mutate(Parcels_Percent=num_parcels/sum(num_parcels), .after=2) |> 
  mutate(Units_Percent=num_units/sum(num_units), .after=4)

zoning_table_data |> 
  bind_rows(zoning_table_data |> summarize(across(-Zone, sum)) |> 
              mutate(Zone='Total', .before=1)) |>
  rename(Parcels_Count=num_parcels,
         Units_Count=num_units) |> 
  gt() |> 
  tab_spanner_delim('_') |> 
  tab_header(title='Cambridge residential parcels and units by zoning type') |> 
  tab_source_note('Data: Cambridge Open Data | Analysis: Kent S Johnson') |> 
  fmt_percent(columns=c(3, 5), decimals=0) |> 
  fmt_integer(columns=c(2, 4)) |> 
  tab_style(locations=cells_body(rows=Zone %in% c('Total')), 
          style=list(cell_text(weight='bold'), 
                     cell_borders(sides='top', weight=px(2))))

