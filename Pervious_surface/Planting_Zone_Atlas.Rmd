---
title: "Planting Zone Atlas"
author: "Kent Johnson for Green Cambridge"
date: '`r Sys.Date()`'
output: 
  kjutil::small_format:
    toc: true
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE}
knitr::opts_chunk$set(echo=FALSE,fig.width=10, fig.height=10, comment=NA, warning=FALSE, message=FALSE)

library(tidyverse)
library(sf)
library(ggmap)
library(ggspatial)
library(rosm)
```

```{r data}
# Cambridge City boundaries
city = read_sf(here::here(
  'Cambridge Open Data/Shapefiles/BOUNDARY_CityBoundary.shp/BOUNDARY_CityBoundary.shp'), quiet=TRUE) %>%
  st_transform(st_crs('OGC:CRS84'))

# Assessor's parcels containing pervious surface
pervious_parcels = st_read(here::here(
  'Pervious_surface/Pervious_parcels_w_zone.gpkg'), quiet=TRUE) %>% 
  st_transform(st_crs('OGC:CRS84'))

# Actual pervious surface
pervious_split = st_read(here::here('Pervious_surface/Pervious_split.gpkg'), quiet=TRUE) %>% 
  st_transform(st_crs('OGC:CRS84')) %>% 
  filter(area>=500, radius >=10)

# "Planting zones" from Green Cambridge
zones = st_read(here::here('Pervious_surface/Cambridge_Pervious_Properties_Zones/Parcels_Polys_fixed.gpkg'), quiet=TRUE) %>% 
  st_transform(st_crs('OGC:CRS84'))
```

## Overview

```{r overview}
ggplot() +
  annotation_map_tile(type='cartolight', zoomin=-1, progress='none') +
  geom_sf(data=city, color='steelblue', size=1, fill=NA) +
  geom_sf(data=zones, color='red', fill=NA) +
  geom_sf_text(data=zones, aes(label=Id)) +
  annotation_north_arrow(location='br') +
  theme(axis.ticks=element_blank(), axis.text=element_blank()) +
  labs(title='Locations of planting zones',
       caption='Map: Kent Johnson for Green Cambridge | Data: Cambridge GIS\nBase map © OpenStreetMap contributors and CARTO')

```

```{r zone_maps, results='asis'}
# Make a nice map of a single zone
zone_map = function(zone_id) {
  zone = zones %>% filter(Id==zone_id)
  parcels = pervious_parcels %>% filter(Id==zone_id)
  pervious = pervious_split[
    st_within(pervious_split, zone, sparse=FALSE)[,1],]
  
  title = str_glue('Planting Zone {zone_id} - {nrow(parcels)} parcels')
  # Get basemap tiles
  # read_osm doesn't work, it requires an old version of Java
  # tm_basemap doesn't work, it is only for interactive maps
  zone_bbox = st_bbox(zone)
  pct = 0.1
  zone_xrange = zone_bbox$xmax - zone_bbox$xmin
  zone_xlim = c(zone_bbox$xmin-zone_xrange*pct, zone_bbox$xmax+zone_xrange*pct)
  zone_yrange = zone_bbox$ymax - zone_bbox$ymin
  zone_ylim = c(zone_bbox$ymin-zone_yrange*pct, zone_bbox$ymax+zone_yrange*pct)
  
  ggplot() +
    annotation_map_tile(type='cartolight', zoomin=0, progress='none') +
    geom_sf(data=zone, color='red', fill=NA) +
    geom_sf(data=pervious, color='green', fill='green') +
    geom_sf(data=parcels, color='grey50', fill=NA) +
    annotation_north_arrow(location='br') +
    coord_sf(xlim=zone_xlim, ylim=zone_ylim, crs=st_crs('OGC:CRS84')) +
    theme(axis.ticks=element_blank(), axis.text=element_blank()) +
    labs(title=title,
         caption='Map: Kent Johnson for Green Cambridge | Data: Cambridge GIS\nBase map © OpenStreetMap contributors and CARTO')
}

for (zone_id in 1:49) {
  cat('\n\n\\newpage\n\n## Planting Zone ', zone_id, '\n\n', sep='')
  print(zone_map(zone_id))
}
```

