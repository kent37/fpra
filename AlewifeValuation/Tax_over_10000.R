# How many properties have > 10,000 tax?
# This is wrong for 2018, there is no 2018 residential exemption info!!

source('~/Google Drive/FPRA/AlewifeValuation/ExploreTaxRevenue.R')
assess = get_assess(2018)
year=2018

# Get the tax function...

target_assess = assess %>%
filter(!tax_exempt(as.character(StateClassCode))) %>%
mutate(RC = if_else(startsWith(as.character(StateClassCode), '1'), 'Residential', 'Commercial')) %>%
mutate(Tax=tax(AssessedValue, RC=='Commercial', ResidentialExemption)) %>%
filter(is.na(Tax) | Tax > 0) %>% filter(RC=='Residential')

sngl = target_assess %>% filter(PropertyClass=='SNGL-FAM-RES')
summary(sngl$Tax)
sum(sngl$Tax>10000)/nrow(sngl)
sum(sngl$Tax>10000)
nrow(sngl)

qplot(sngl$Tax, binwidth=500, fill=sngl$Tax>10000) + 
  scale_x_continuous(labels=dollar, limits=c(0, 40000)) + 
  labs(x='Estimated tax', y='Number of properties', fill='Tax > $10,000', 
       title=glue::glue('Estimated {year} property tax for Cambridge single family properties')) + 
  scale_fill_manual(values=c('TRUE'='red', 'FALSE'='green4'))
