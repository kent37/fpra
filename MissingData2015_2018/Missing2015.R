# Show missing 2015 tax data
library(tidyverse)
library(sf)

# Download and read parcel data
temp = 'dl.zip'
parcel_source = 'http://gis.cambridgema.gov/download/shp/ASSESSING_ParcelsFY2015.shp.zip'
download.file(parcel_source, temp)
unzip(temp)
parcels = st_read('ASSESSING_ParcelsFY2015.shp')

# Download and read assessment data
assess = read_csv('https://data.cambridgema.gov/api/views/crnm-mw9n/rows.csv?accessType=DOWNLOAD')

# Find parcels that are not in the assessment data
parcels_not_in_assess = parcels %>% anti_join(assess, by=c(ML='GIS ID'))

# Remove roads and railroads
parcels_not_in_assess = parcels_not_in_assess %>% filter(!ML %in% c('ROAD', 'RR', '-R'))

# Visualize the missing parcels
mapview::mapview(parcels_not_in_assess, zcol='ML')

# Write a shapefile
st_write(parcels_not_in_assess, 'Missing_2015.shp')

# Now the other way around - assessor's properties not in parcels
assess_not_in_parcels = assess %>% anti_join(parcels, by=c('GIS ID'='ML'))

# Mostly, but not entirely, assessor's data that is missing GIS ID
write_csv(assess_not_in_parcels, 'Missing_2015.csv')
