LIHTC data is sourced from
http://lihtc.huduser.org/

Low-Income Housing Tax Credit
Not every listed field is actually available; some of them prevent any report generation. The HTML and CSV files here represent all the data I could get from the site.

More info here:
http://www.huduser.org/portal/datasets/lihtc.html

According to http://en.wikipedia.org/wiki/Low-Income_Housing_Tax_Credit
http://www.nytimes.com/2012/12/21/opinion/a-tax-credit-worth-preserving.html?_r=1
Created by Congress in 1986, the credit is available to investors prepared to sink money into new or rehabilitated low-income housing. It is responsible for about 90 percent of all the affordable housing that is built in this country, and has provided more than 2.5 million rental units since its inception.