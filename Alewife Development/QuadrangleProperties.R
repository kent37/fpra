library(tidyverse)
library(leaflet)
library(sf)
quad = st_read('Alewife Development/Quad/Quadrangle.shp')

source('Cambridge Open Data/CODutil.R')

props = read_csv('Cambridge Open Data/Cambridge_Property_Database_FY2021.csv') %>% 
  parse_location(Address) %>% 
  rename(Address='Location') %>% 
  st_as_sf(coords=c('lon', 'lat'), crs=4326, remove=FALSE)

quad_props = props[quad,]

mapview(quad_props, label='Address', cex=2) +
  mapview(quad, color='black', alpha.regions=0)

write_csv(st_drop_geometry(quad_props),
          'Alewife Development/QuadrangleProperties2021.csv')

leaflet(quad_props) %>% 
  addTiles() %>% 
  addCircleMarkers(radius=1) %>% 
  addPolygons(data=quad, fill=FALSE, color='black')
