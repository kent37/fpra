﻿<metadata>
  <idinfo>
    <citation>
      <citeinfo>
        <title>Level 3 Parcels - Taxable Property Boundaries with Assessor Database Information</title>
        <geoform>vector digital data</geoform>
      </citeinfo>
    </citation>
    <descript>
      <abstract>MassGIS' Level 3 Assessors’ Parcel Mapping data set was developed through a competitive procurement funded by MassGIS. Each community in the Commonwealth was bid on by one or more vendors and the unit of work awarded was a city or town. The specification for this work was Level 3 of the MassGIS Digital Parcel Standard (http://www.mass.gov/mgis/standards.htm#Parstandard). By June 2012, standardization of assessor parcel mapping for at least 215 of Massachusetts' 351 cities and towns will have been completed. MassGIS expects to continue this project in July of 2012 with the objective of completing statewide standardized parcel mapping by the end of June 2013. Full metadata may be viewd at http://www.mass.gov/mgis/L3parcels.htm. This layer is based on a spatial view relating the L3_TAXPAR_POLY feature class to the L3_ASSESS table. In case where multiple records are associated with one parcel, there will be "stacked" polygons.</abstract>
      <purpose>This feature class comprises polygons or multi-part polygons, each of which links to one or more assessor tax records (unless it is a feature for which a tax record has not been established, i.e. public right-of-way, most water, etc…). In situations where two or more contiguous parcels have common ownership, the internal boundaries of these parcels have been removed, creating a single polygon corresponding to the tax listing that it represents.  Where two or more non-contiguous parcel polygons share common ownership, they have been converted to a multi-part polygon; each multi-part polygon links to one or more assessor tax listings.</purpose>
    </descript>
    <status>
      <update>As needed</update>
    </status>
    <spdom>
      <bounding>
        <westbc>-73.503998</westbc>
        <eastbc>-69.912838</eastbc>
        <northbc>42.834135</northbc>
        <southbc>41.298888</southbc>
      </bounding>
    </spdom>
    <keywords />
    <accconst>None</accconst>
    <useconst>None</useconst>
    <datacred>Commonwealth of Massachusetts Office of Geographic Information (MassGIS)</datacred>
    <native>Microsoft Windows Server 2008 R2 Version 6.1 (Build 7600) ; ESRI ArcGIS 10.0.2.3200</native>
  </idinfo>
  <spdoinfo>
    <direct>Vector</direct>
    <ptvctinf>
      <sdtsterm>
        <sdtstype>GT-polygon composed of chains</sdtstype>
        <ptvctcnt>808601</ptvctcnt>
      </sdtsterm>
    </ptvctinf>
  </spdoinfo>
  <spref>
    <horizsys>
      <planar>
        <mapproj>
          <mapprojn>NAD 1983 StatePlane Massachusetts Mainland FIPS 2001</mapprojn>
          <lambertc>
            <stdparll>41.71666666666667</stdparll>
            <stdparll>42.68333333333333</stdparll>
            <longcm>-71.5</longcm>
            <latprjo>41.0</latprjo>
            <feast>200000.0</feast>
            <fnorth>750000.0</fnorth>
          </lambertc>
        </mapproj>
        <planci>
          <plance>coordinate pair</plance>
          <coordrep>
            <absres>0.0001</absres>
            <ordres>0.0001</ordres>
          </coordrep>
          <plandu>Meter</plandu>
        </planci>
      </planar>
      <geodetic>
        <horizdn>D North American 1983</horizdn>
        <ellips>GRS 1980</ellips>
        <semiaxis>6378137.0</semiaxis>
        <denflat>298.257222101</denflat>
      </geodetic>
    </horizsys>
  </spref>
  <eainfo>
    <detailed>
      <enttyp>
        <enttypl>GISDATA.L3_TAXPAR_POLY_ASSESS</enttypl>
        <enttypd>See http://www.mass.gov/mgis/Parstndrd_Ver2_0.pdf for more details</enttypd>
      </enttyp>
      <attr>
        <attrlabl>OBJECTID</attrlabl>
        <attrdef>Internal feature number.</attrdef>
        <attrdefs>ESRI</attrdefs>
        <attrdomv>
          <udom>Sequential unique whole numbers that are automatically generated.</udom>
        </attrdomv>
      </attr>
      <attr>
        <attrlabl>MAP_PAR_ID</attrlabl>
        <attrdef>The parcel ID that appears on the assessor’s map</attrdef>
      </attr>
      <attr>
        <attrlabl>LOC_ID</attrlabl>
        <attrdef>Uniquely identifies (statewide) a tax parcel polygon.</attrdef>
      </attr>
      <attr>
        <attrlabl>POLY_TYPE</attrlabl>
        <attrdef>Identifies the kind of polygon in the tax parcel layer. Most polygons will be coded “FEE”; those representing dissolved parcels will be coded “TAX”.</attrdef>
        <attrdomv>
          <edom>
            <edomv>FEE</edomv>
            <edomvd>Parcels with records in the related assessor database</edomvd>
          </edom>
        </attrdomv>
        <attrdomv>
          <edom>
            <edomv>TAX</edomv>
            <edomvd>Dissolved parcels with records in the related assessor database</edomvd>
          </edom>
        </attrdomv>
        <attrdomv>
          <edom>
            <edomv>WATER</edomv>
            <edomvd>Parcels having boundaries which are coincident with the shoreline of a water feature not entirely contained within one parcel</edomvd>
          </edom>
        </attrdomv>
        <attrdomv>
          <edom>
            <edomv>ROW</edomv>
            <edomvd>Public right-of-way polygon that does not overlap tax parcel polygons</edomvd>
          </edom>
        </attrdomv>
        <attrdomv>
          <edom>
            <edomv>PRIV_ROW</edomv>
            <edomvd>Private right-of-way</edomvd>
          </edom>
        </attrdomv>
        <attrdomv>
          <edom>
            <edomv>RAIL_ROW</edomv>
            <edomvd>Railroad right-of-way</edomvd>
          </edom>
        </attrdomv>
      </attr>
      <attr>
        <attrlabl>MAP_NO</attrlabl>
        <attrdef>Number of the assessor’s map sheet from which the mapping of the parcel in the digital file was created</attrdef>
      </attr>
      <attr>
        <attrlabl>SOURCE</attrlabl>
        <attrdef>Boundary feature source</attrdef>
        <attrdomv>
          <edom>
            <edomv>ASSESS</edomv>
            <edomvd>Assessor map</edomvd>
          </edom>
        </attrdomv>
        <attrdomv>
          <edom>
            <edomv>SUBDIV</edomv>
            <edomvd>Subdivision plan</edomvd>
          </edom>
        </attrdomv>
        <attrdomv>
          <edom>
            <edomv>ANR</edomv>
            <edomvd>Subdivision approval not required</edomvd>
          </edom>
        </attrdomv>
        <attrdomv>
          <edom>
            <edomv>ROAD LAYOUT</edomv>
            <edomvd>Road layout plan</edomvd>
          </edom>
        </attrdomv>
        <attrdomv>
          <edom>
            <edomv>OTHER</edomv>
            <edomvd>Other sources</edomvd>
          </edom>
        </attrdomv>
      </attr>
      <attr>
        <attrlabl>PLAN_ID</attrlabl>
        <attrdef>Identifying information for plan (e.g, subdivision or road plan) used to update the digital file</attrdef>
      </attr>
      <attr>
        <attrlabl>LAST_EDIT</attrlabl>
        <attrdef>The date this parcel polygon was last edited, formatted as YYYYMMDD. Initial value will be the date the GIS file was brought to compliance with this standard.</attrdef>
      </attr>
      <attr>
        <attrlabl>BND_CHK</attrlabl>
        <attrdef>Identifies parcels where, although there is a discrepancy between the parcel boundary and features visible on the orthoimage base map, the boundary shown is believed to be correct. In addition, this attribute will enable those conducting QA to identify parcels where the boundary compilation may need editing.</attrdef>
        <attrdomv>
          <edom>
            <edomv>Null</edomv>
            <edomvd>Indicates that no particular attention has been given to checking the compilation of the given parcel. Most parcels would simply carry null values in this field.</edomvd>
          </edom>
        </attrdomv>
        <attrdomv>
          <edom>
            <edomv>CC</edomv>
            <edomvd>The compilation has been checked and will be entered by the compiler to indicate an apparent discrepancy between the map data and the orthoimage base map where, in their professional judgment and based on the available evidence, the compilation is correct.</edomvd>
          </edom>
        </attrdomv>
        <attrdomv>
          <edom>
            <edomv>NR</edomv>
            <edomvd>Entered by the person performing QA to tag parcel polygons where boundary compilation needs review with correction or justification by the original editor</edomvd>
          </edom>
        </attrdomv>
        <attrdomv>
          <edom>
            <edomv>OK</edomv>
            <edomvd>Entered by MassGIS to indicate that the discrepancy between the boundary compilation and the orthoimagery is consistent with known information. If a polygon coded in this way is subsequently edited, this attribute would be changed to null or “CC”.</edomvd>
          </edom>
        </attrdomv>
      </attr>
      <attr>
        <attrlabl>NO_MATCH</attrlabl>
        <attrdef>Identifies parcel polygons whose exclusion from calculations of Level III match rates between parcel polygons and the assessor’s tax list has been approved by MassGIS.</attrdef>
        <attrdomv>
          <edom>
            <edomv>N</edomv>
            <edomvd>Default value</edomvd>
          </edom>
        </attrdomv>
        <attrdomv>
          <edom>
            <edomv>Y</edomv>
            <edomvd>Identifies parcels approved for exclusion from the match</edomvd>
          </edom>
        </attrdomv>
      </attr>
      <attr>
        <attrlabl>TOWN_ID</attrlabl>
        <attrdef>Town-ID from MassGIS towns data layer (1-351)</attrdef>
      </attr>
      <attr>
        <attrlabl>SHAPE</attrlabl>
        <attrdef>Feature geometry.</attrdef>
        <attrdefs>ESRI</attrdefs>
        <attrdomv>
          <udom>Coordinates defining the features.</udom>
        </attrdomv>
      </attr>
      <attr>
        <attrlabl>SITE</attrlabl>
        <attrdef>Name of a larger site (e.g. campus) that comprises multiple parcel polygons. Maintained by MassGIS and not part of the Standard.</attrdef>
      </attr>
      <attr>
        <attrlabl>ESN</attrlabl>
        <attrdef>Emergency Service Zone Number. Maintained by MassGIS and not part of the Standard.</attrdef>
      </attr>
      <attr>
        <attrlabl>LU_CODES</attrlabl>
        <attrdef>Land use type. Maintained by MassGIS and not part of the Standard.</attrdef>
      </attr>
      <attr>
        <attrlabl>DEV</attrlabl>
        <attrdef>Indicates whether a parcel has been developed. Maintained by MassGIS and not part of the Standard.</attrdef>
      </attr>
      <attr>
        <attrlabl>SYM1</attrlabl>
        <attrdef>General land use symbology code. Maintained by MassGIS and not part of the Standard.</attrdef>
      </attr>
      <attr>
        <attrlabl>SYM2</attrlabl>
        <attrdef>Detailed land use symbology code. Maintained by MassGIS and not part of the Standard.</attrdef>
      </attr>
      <attr>
        <attrlabl>SHAPE.AREA</attrlabl>
      </attr>
      <attr>
        <attrlabl>SHAPE.LEN</attrlabl>
      </attr>
    </detailed>
  </eainfo>
  <distinfo>
    <distrib>
      <cntinfo>
        <cntorgp>
          <cntorg>MassGIS</cntorg>
        </cntorgp>
        <cntaddr>
          <addrtype>Unknown</addrtype>
          <address>One Ashburton Place, Room 1601</address>
          <city>Boston</city>
          <state>MA</state>
          <postal>02108</postal>
          <country>USA</country>
        </cntaddr>
        <cntvoice>617-619-5615</cntvoice>
      </cntinfo>
    </distrib>
    <distliab>See access and use constraints information.</distliab>
    <stdorder>
      <digform>
        <digtinfo>
          <formname>Shapefile and Geodatabase</formname>
        </digtinfo>
      </digform>
      <ordering>Download from http://www.mass.gov/mgis/ftpL3parcels.htm</ordering>
    </stdorder>
  </distinfo>
  <metainfo>
    <metd>20120207</metd>
    <metstdn>FGDC Content Standard for Digital Geospatial Metadata</metstdn>
    <metstdv>FGDC-STD-001-1998</metstdv>
  </metainfo>
</metadata>