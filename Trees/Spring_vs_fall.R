# Look at spring vs fall planting using 2016-2022 planting data

# Gross mortality of spring 2020 - fall 2021
planted = planted %>% 
  mutate(spring_planting=(month(PlantDate)<=8 & month(PlantDate) >=3),
         PlantSeason = if_else(spring_planting, 'Spring', 'Fall'),
         Removed=Removed | (!is.na(WateringRe) & str_starts(WateringRe, 'x')),
         year_season = paste(PlantSeason, if_else(month(PlantDate)<3, PlantYear-1, PlantYear)))


recent_fall = planted %>% 
  filter((PlantYear == 2020 & month(PlantDate) >=3) 
         | PlantYear==2021 
         | (PlantYear==2022 & month(PlantDate)<3))

# Why are these so different???
planted %>% filter(!year_season %in% c('Fall 2016', 'Spring 2016')) %>% 
  fit_survival_stratified('year_season') -> fit_data
plot_survival_stratified(fit_data$fit, fit_data$plot_data, conf.int=TRUE,
  'Planting year and season', vline_at=c(12, 24, 36))

fit_data = fit_survival_stratified(recent_fall, 'PlantSeason')
plot_survival_stratified(fit_data$fit, fit_data$plot_data, conf.int=TRUE,
  'Planting season', vline_at=c(12, 24, 36))

fit_data = fit_survival_stratified(recent_fall, 'year_season')
plot_survival_stratified(fit_data$fit, fit_data$plot_data, conf.int=TRUE,
  'Planting year and season', vline_at=c(12, 24, 36))


# No fall 2022 trees have been removed
planted %>% filter(PlantYear==2022, !spring_planting) %>% count(Removed)

recent_fall %>% 
  group_by(year_season) %>% 
  summarize(Count=n(), Survived=sum(!Removed), Removed=sum(Removed),
            Mortality=int_pct(Removed/Count)) %>% 
  gt() %>% 
  cols_label(year_season='Year & season') %>% 
  tab_header(title='Planting summary, Spring 2020 - Fall 2021')

# Without the NA PlantingSe:
recent_fall %>% 
  filter(!is.na(PlantingSe)) %>% 
  group_by(PlantSeason) %>% 
  summarize(Count=n(), Survived=sum(!Removed), Removed=sum(Removed),
            Mortality=int_pct(Removed/Count)) %>% 
  gt() %>% 
  tab_header(title='Planting summary, Fall 2020 - Fall 2022, PARTIAL')

# Gross mortality of fall 2019 - fall 2021
older_fall = planted %>% 
  filter((PlantYear >= 2020 | (PlantYear==2019 & !spring_planting))
         & (PlantYear != 2022))

older_fall %>% 
  group_by(PlantSeason) %>% 
  summarize(Count=n(), Survived=sum(!Removed), Removed=sum(Removed),
            Mortality=int_pct(Removed/Count)) %>% 
  gt() %>% 
  tab_header(title='Planting summary, Fall 2019 - Fall 2021')
