# Sidewalks at least six feet wide and retired tree wells they contain
library(tidyverse)
library(leaflet)
library(leafgl)
library(sf)

sidewalks = st_read(here::here('Cambridge Open Data/Shapefiles/BASEMAP_Sidewalks.shp/BASEMAP_Sidewalks.shp'))

combined = st_union(st_make_valid(sidewalks))

# Sidewalks at least six feet wide
six_feet = st_buffer(combined, -3, endCapStyle='SQUARE', joinStyle='BEVEL') %>% 
  st_buffer(3, endCapStyle='SQUARE', joinStyle='BEVEL')

st_write(combined, delete_layer=TRUE, layer='all',
         here::here('Trees/Lost_and_planted/sidewalks.gpkg'))
st_write(six_feet, delete_layer=TRUE, layer='six_feet',
         here::here('Trees/Lost_and_planted/sidewalks.gpkg'))
combined = st_read(here::here('Trees/Lost_and_planted/sidewalks.gpkg'), 
                   layer='all')
six_feet = st_read(here::here('Trees/Lost_and_planted/sidewalks.gpkg'), 
                   layer='six_feet')

# Retired sites within the six-foot sidewalks
retired_sites = st_read(here::here('Trees/Lost_and_planted/lost_tree.gpkg')) %>% 
  filter(CurrentSiteType=='Retired' & !Replaced)

# This is slow!
retired_in_six_feet = st_within(retired_sites, st_union(six_feet), sparse=FALSE)
retired_sites$in_six_feet = retired_in_six_feet[,1]
st_write(retired_sites, delete_layer=TRUE, layer='retired_in_six_feet',
         here::here('Trees/Lost_and_planted/sidewalks.gpkg'))

# Get current data for the retired sites. Don't do any cleaning.
retired_info = st_read(
  "/Users/kent/Dev/CambridgeTrees/ENVIRONMENTAL_StreetTrees_2021_10_04.shp") %>% 
  filter(TreeID %in% (retired_sites %>% filter(in_six_feet) %>% pull(TreeID)))


meters_per_foot = 0.3048
leaflet() %>% 
  addProviderTiles('CartoDB.Positron') %>% 
  setView(-71.117, 42.378, zoom = 14) %>% 
  addPolygons(data=st_transform(combined, 4326), 
              group='All sidewalks', stroke=FALSE,
              fillColor='gray', fillOpacity=1) %>% 
  addPolygons(data=st_transform(six_feet, 4326),
              group='Six foot sidewalks', stroke=FALSE,
              fillColor='green', fillOpacity=1) %>% 
  addCircles(data=retired_sites %>% filter(in_six_feet) %>% st_transform(4326), 
             popup=~paste0('#', TreeID, '<br>', StreetNumber, ' ', StreetName),
             color='black', group='Retired tree wells',
             fillOpacity=1, stroke=FALSE,
             radius=2*meters_per_foot*5) %>% 
  addLayersControl(overlayGroups=c('All sidewalks', 'Six foot sidewalks',
                                   'Retired tree wells'),
                   options=layersControlOptions(collapsed=FALSE))
