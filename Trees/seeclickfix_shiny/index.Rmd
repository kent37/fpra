---
title: "Cambridge SeeClickFix Requests"
author: "Kent Johnson"
output: 
  html_document:
    theme: cerulean
runtime: shiny
---

```{r setup, echo=FALSE,include=FALSE,message=FALSE}
knitr::opts_chunk$set(echo=FALSE,fig.width=8, fig.height=6, comment=NA, warning=FALSE, message=FALSE)
```

```{r}
# Cambridge tree planting requests

library(tidyverse)
library(glue)
library(htmltools)
library(leaflet)
library(lubridate)
library(purrrlyr)
library(sp)

token = Sys.getenv('SOCRATA_CAMBRIDGE')
topics = read_csv(paste0('https://data.cambridgema.gov/resource/pc4i-yfqg.csv?$group=issue_type,ticket_status&$select=issue_type,ticket_status,count(issue_type)&$$app_token=', token)) %>% 
  spread(ticket_status, count_issue_type, fill=0) %>% 
  mutate(total=open+closed) %>% 
  arrange(desc(open))

choices = topics$issue_type %>% 
  set_names(glue_data(topics, '{issue_type} ({open} open/{closed} closed)'))
query_limit=max(topics$total)

selectInput('topic', 'Select a topic:', choices, width='400px')

tickets = reactive({
  tp = read_csv(paste0('https://data.cambridgema.gov/resource/pc4i-yfqg.csv?issue_type=', URLencode(input$topic), '&$limit=', query_limit, '&$$app_token=', token)) %>% 
  mutate(
    time_to_close=difftime(ticket_closed_date_time,
                           ticket_created_date_time, units='days'),
    time_open=now()-ticket_created_date_time,
    time=ifelse(ticket_status=='open', time_open, time_to_close))

  # htmltools is nice but this is slow; just use paste
  # tp = by_row(tp,
  #      function(row) {
  #        HTML(as.character(p(
  #          row$issue_description, br(),
  #          row$address, br(),
  #          'Opened: ', as.Date(row$ticket_created_date_time), br(),
  #          'Closed: ', as.Date(row$ticket_closed_date_time), br(),
  #          a(href=paste0('https://seeclickfix.com/issues/',
  #                       row$ticket_id),
  #                       target="_blank" , 'Ticket'), br()
  #        )))}, .to='popup')

  tp = tp %>% 
  mutate(color=ifelse(ticket_status=='open', 'red', 'green'),
         size=ifelse(ticket_status=='open', 
                     as.numeric(time_open), 
                     as.numeric(time_to_close)))

coordinates(tp) = ~lng+lat
proj4string(tp) = "+init=EPSG:4326"
tp
})

popups = reactive({with(tickets()@data, paste0(issue_description, '<br>',
                           address, '<br>',
                           'Opened: ', as.Date(ticket_created_date_time), '<br>',
                           'Closed: ', as.Date(ticket_closed_date_time), '<br>',
                           '<a href="https://seeclickfix.com/issues/', 
                         ticket_id, '" target="_blank">Ticket</a>') %>% map(HTML))
})

theme_set(theme_bw())
                 
tickets_open = reactive({tickets()@data %>% filter(ticket_status=='open') %>% 
  summarize(Count=n(), 
            avg=mean(time_open) %>% as.numeric %>% round(0),
            q90 = quantile(time_open, 0.9) %>% as.numeric %>% round(0))
})

tickets_closed = reactive({tickets()@data %>% filter(ticket_status=='closed') %>% 
  summarize(Count=n(), 
            avg=mean(time_to_close) %>% as.numeric %>% round(0),
            q90 = quantile(time_to_close, 0.9) %>% as.numeric %>% round(0))
})
```

### Summary

As of `r format(Sys.time(), tz='US/Eastern', usetz=TRUE)`, 
there are `r renderText({nrow(tickets())})` `r renderText({input$topic})`
items in the 
[Cambridge SeeClickFix database](https://data.cambridgema.gov/Public-Works/Commonwealth-Connect-Service-Requests/2z9k-mv9g). 
Of these, `r renderText({tickets_open()[['Count']][1]})` are open requests 
and `r renderText({tickets_closed()[['Count']][1]})` are closed.

The **average time to close** a `r renderText({input$topic})` ticket is 
`r renderText({tickets_closed()[['avg']][1]})` days. 90% of closed 
`r renderText({input$topic})` tickets were closed in 
`r renderText({tickets_closed()[['q90']][1]})` days.

The **average time open** tickets have been open is 
`r renderText({tickets_open()[['avg']][1]})` days. 
90% of open `r renderText({input$topic})` tickets 
have been open less than 
`r renderText({tickets_open()[['q90']][1]})` days.

Note: View the individual requests for details.

### Time in System

This figure shows the time `r renderText({input$topic})` tickets stay open in SeeClickFix.

<span style='color:red'>**Open requests are red**</span> and show the time the request has been open.  
<span style='color:green'>**Closed requests are green**</span> and show the time to close.

```{r}
output$plot = renderPlot({
  limit = max(tickets()@data$size)
  ggplot(tickets()@data) + 
  geom_histogram(aes(time, fill=ticket_status), binwidth=10, position='dodge') +
  scale_x_continuous(breaks=seq(0, limit, 50), minor_breaks=seq(10, limit, 10)) +
  labs(title=paste(input$topic, 'in SeeClickFix'),
       subtitle='Time in system (days), Open requests in red, closed requests in green',
       x='Days in system', y='Number of requests',
       fill='Ticket status') +
  scale_fill_manual(values=c(open='red', closed='green3')) +
  facet_wrap(~ticket_status, ncol=1)
})

plotOutput('plot', width=800, height=600)
```

### Location of requests

This map shows the location of all `r renderText({input$topic})` tickets. 
Larger circles have longer response times. Click on a circle
for more information.

```{r}
# Let's make a map!
output$map = renderLeaflet({map = leaflet() %>% 
  addTiles("http://tiles.mapc.org/basemap/{z}/{x}/{y}.png",
           attribution='Map tiles by <a href="http://mapc.org">MAPC</a>, Data by <a href="http://www.mass.gov/mgis/">MassGIS</a>.') %>% 
  setView(-71.1129096, 42.3783904, zoom = 14)

map %>% addCircles(data=tickets(), popup=popups(), label=~address, fillColor=~color, 
                   stroke=FALSE, fillOpacity=0.5, 
                   radius=~pmax(20, size/10))
})

leafletOutput('map', width='95%', height=600)

```

<small>Copyright 2017 Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle;display:inline" 
  src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" target="_blank"/></a>
  <span style='float:right;font-style: italic;'>`r Sys.Date()`</span></small>