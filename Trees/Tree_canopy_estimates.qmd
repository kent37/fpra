---
title: "Estimates of Cambridge Tree Canopy"
author: "Kent Johnson"
toc: true
callout-icon: false
format:
  html:
    embed-resources: true
    header-includes: '<link rel="icon" type="image/png" href="icons8-oak-tree-48.png">'
execute:
  echo: false
  message: false
  warning: false
---

```{r setup, include=FALSE}
library(tidyverse)
library(gt)

knitr::opts_chunk$set(echo=FALSE,fig.width=8, fig.height=6, comment=NA, results='asis')
```

The City of Cambridge has published multiple estimates of tree canopy coverage
for the years 2009, 2014, 2018 and 2020. This report compares the estimates
from the various sources and years.

## Sources

Tree canopy estimates are available in both published reports and
in data files available from Cambridge GIS.

#### Published reports

The published reports include

- A Report on the City of Cambridge’s Existing and Possible Tree Canopy
  - Published June 1, 2012
  - Canopy estimate for 2009
- Tree Canopy in Cambridge, MA: 2009-2014
  - Published February 2, 2018
  - Canopy estimates (in chart only) for 2009 and 2014
- Tree Canopy Assessment, Cambridge, MA
  - Published February 19, 2020
  - Canopy estimates for 2014 and 2018
- Tree Canopy Assessment, Cambridge, MA 2009-2020
  - Published October 12, 2022
  - Canopy estimates for 2009, 2014, 2018 and 2020

With the exception of the report published in 2018, these reports include 
canopy estimates in acres in the text of the report. The 2018 report
only reports the estimates in a chart. For this report, the chart was
digitized to obtain the estimates.

### GIS data

The [GIS data sets available](https://www.cambridgema.gov/GIS/gisdatadictionary/Environmental) include

- Tree Canopy 2009 (data for 2009)
- Tree Canopy 2014 (data for 2014)
- Tree Canopy 2018 (data for 2018)
- Tree Canopy Change 2009 2014 (data for 2009 and 2014)
- Tree Canopy Change 2014 2018 (data for 2014 and 2018)
- Tree Canopy 2022 (data for 2009, 2014, 2018 and 2020)

The GIS data sets are shapefiles which show the actual outline of the tree
canopy. The area of the outlines gives an estimate of the total canopy area.
With the exception of the 2022 data set, the published data includes portions
of canopy data which are outside the Cambridge City limits. For this report,
the outlines were clipped to the City limits before calculating area.

```{r data}
# All the estimates, from Explore_tree_canopy.R
estimates = tribble(
  ~Source, ~`2009`, ~`2014`, ~`2018`, ~`2020`,
  '2012 report', 1222, NA, NA, NA,
  '2009 data, trimmed', 1220, NA, NA, NA,
  '2018 report (est)', 1067+204, 1067+106, NA, NA,
  '2020 report', 1222, 1151, 1040, NA,
  '2014 data, trimmed', NA, 1167, NA, NA,
  '2014-2018 data, trimmed', NA, 1151, 1040, NA,
  '2018 data, trimmed', NA, NA, 1040, NA,
  '2022 report', 1092, 944, 1012, 1045,
  '2020 data', 1092,  944, 1012, 1045
) |> 
    mutate(is_report=if_else(str_detect(Source, 'report'), 'Report', 'Data')) 
```

## Tree canopy estimates

This table gives the available tree canopy estimates from each of the above sources.

```{r table}
# Table
estimates |> 
  select(-is_report) |> 
  gt() |> 
  sub_missing(missing_text='-') |> 
  tab_spanner('Year', columns=2:5) |> 
  tab_header(title='Estimates of Cambridge tree canopy size',
       subtitle='Values from published reports and GIS data sets') |> 
  tab_source_note('Data: City of Cambridge | Analysis: Kent S Johnson')
```

This chart shows the estimates graphically.

```{r plot}
# Plot
# Lookup table for legend
source2report <- setNames(estimates$is_report, estimates$Source)
report2shape <- c("Report" = 16, "Data" = 17)

estimates |> 
  pivot_longer(cols=c(-Source, -is_report)) |> 
  filter(!is.na(value)) |> 
  mutate(year=factor(name)) |> 
  ggplot(aes(year, value, color=Source, shape=is_report)) +
    geom_jitter(width = 0.2, height = 0, size=4) +
  scale_colour_manual(
    breaks = names(source2report),
    values=cols4all::c4a('tableau.10', 9),
    guide  = guide_legend(
      override.aes = list(shape = report2shape[source2report])
  )) +
  scale_shape_manual(values = report2shape, guide = "none") +
  labs(x='Year', y='Estimated tree canopy (acres)',
       title='Estimates of Cambridge tree canopy size',
       subtitle='Values from published reports and GIS data sets',
       caption='Data: City of Cambridge | Analysis: Kent S Johnson') +
    theme_minimal(base_size=12)

```

### Conclusions

The 2009 and 2014 estimates from the 2022 report and data are significantly different
from other estimates for those years, and the 2018 estimates are slightly 
different. The 2022 report attributes this to a reprocessing of the earlier data.

With one exception, the estimates from the written reports agree closely
with the estimates from the corresponding data.
The sole exception is the 2009 estimate from the chart in the 
2018 report, which differs somewhat from the other 2009 estimates. 

<small>Copyright `r format(Sys.Date(), '%Y')` Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle;display:inline" 
  src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" target="_blank"/></a>
  <span style='float:right;font-style: italic;'>`r Sys.Date()`</span></small>