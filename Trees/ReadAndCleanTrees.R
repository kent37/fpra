# Read and clean the street tree file
library(dplyr)
library(lubridate)
library(tidyverse)
library(sf)

# Read the most recent tree data
readLatestTrees = function() {
  base_path = "/Users/kent/Dev/CambridgeTrees"
  files = list.dirs(base_path, recursive=FALSE)

  dates = files %>% 
    str_extract('\\d{4}_\\d{2}[_-]\\d{2}')
  latest = dates %>% sort(decreasing=TRUE) %>% `[[`(1)
  
  latest_name = paste0('ENVIRONMENTAL_StreetTrees_', latest, '.shp')
  readAndCleanTrees(file.path(base_path, latest_name))
}

readAndCleanTrees = function(path) {
  trees = st_read(file.path(path, 'ENVIRONMENTAL_StreetTrees.shp'),
                      stringsAsFactors=FALSE, quiet=TRUE)

  # Fix a typo in the 'modified' column (no longer needed)
  #trees$modified[trees$modified=='2106/02/06'] = '2016/02/06'

  # Remove uninformative columns
  #trees = select(trees, -c(TreeWellLe, TreeWellWi, AbutsOpenA, ExposedRoo))
  
  # Rename columns to match the data dictionary
  trees = rename(trees, 
         RemovalDate=RemovalDat, 
         SpeciesShort=SpeciesSho, 
         #TreeCondition=TreeCondit, # No longer provided 7/2021
         OverheadWires=OverheadWi, StreetNumber=StreetNumb, 
         ADACompliant=ADAComplia, 
         TreeGrateActionReq=TreeGrateA, 
         #SpeciesNotes=SpeciesNot, # Field not present as of 10/2017
         TreeWellCover=TreeWellCo)
  
  # Convert dates and numeric columns
  if ('created' %in% names(trees))
    trees$created = ymd(trees$created) # Older records
  else if ('created_da' %in% names(trees))
    trees$created = ymd(trees$created_da) # Current
  trees = trees %>% mutate(
    #modified = ymd(modified),
    trunks = as.numeric(trunks),
    PlantDate = ymd(PlantDate),
    RemovalDate = ymd(RemovalDate),
    TreeID = as.numeric(TreeID)
  )


# These fields are not in older data
if ('TreeWellID' %in% names(trees))
  trees$TreeWellID = as.numeric(trees$TreeWellID)
if ('last_edi_1' %in% names(trees))
  trees$last_edi_1 = ymd(trees$last_edi_1)
  
  # Cleanup
  #trees$Ownership[grepl('Private.*Back of Sidewalk', trees$Ownership, perl=TRUE)] = 'Private - Back of Sidewalk'
  trees$species[is.na(trees$species)] = 'Unknown'
  trees$SpeciesShort[is.na(trees$SpeciesShort)] = 'Unknown'
  
  trees$TreeGrateActionReq = factor(trees$TreeGrateActionReq, 
    labels=c('N/A', 'Priority Removal', 'Priority Maintenance; Good Grate needs Clean-out',
    'Ok Inspect Annually; < 2" clearance', 'Good No Action'))
  
  if ('TreeCondit' %in% names(trees)) {
    trees$TreeCondition = sub(' \\(EW 2013\\)', '', trees$TreeCondit)
    trees$TreeCondition = factor(trees$TreeCondition, 
      levels=c('', 'Dead', 'Poor', 'Fair', 'Good', 'Excellent'))
    trees$TreeCondit = NULL
  }
  
  # The download file no longer has neighborhood...
  # Get it from the neighborhood shapefile
  neighborhoods = st_read(here::here('Cambridge Open Data/Shapefiles/BOUNDARY_CDDNeighborhoods.shp'),
                        stringsAsFactors=FALSE, quiet=TRUE)
  
  tree_nbhd = st_within(trees, neighborhoods)
  tree_nbhd=map_int(as.list(tree_nbhd), ~{if (length(.x)==0) NA_integer_ else .x[1]})
  trees$Neighborhood = neighborhoods$NAME[tree_nbhd]
  
  trees
}

# Sort out the free-text "Location Retired Reason" field
# to fewer categories
classify_retired_reason = function(desc) {
  desc = str_to_lower(desc)
  case_when(
    is.na(desc) ~ 'Unknown',
    str_detect(desc, 'refus|declin|owner|abut|resident') ~ 
      'Owner refused',
    str_detect(desc, 'bus stop') ~ 'Bus stop',
    str_detect(desc, 'stop sign') ~ 'Stop sign',
    str_detect(desc, 'hydrant') ~ 'Hydrant',
    str_detect(desc, 'utilit|gas line|catch basin') ~ 
      'Utility conflict',
    str_detect(desc, 'manhole') ~ 'Manhole',
    str_detect(desc, 'soil contamination') ~ 'Soil contamination',
    str_detect(desc, 'paved|bricked|no tree well|closed|no longer|no well') ~
               'Paved or missing',
    str_detect(desc, 'driveway|curb cut|egress') ~ 'Curb cut',
    str_detect(desc, 'handicap') ~ 'Handicap space',
    str_detect(desc, 'private tree|overhang|private property tree|existing tree|new tree|new well|mature tree') ~ 'Tree conflict',
    str_detect(desc, 'sidewalk|sw|<6|6ft|narrow') ~ 'Sidewalk issue',
    str_detect(desc, 'bike|bicycle') ~ 'Bicycle issue',
    TRUE ~ 'Other'
  )
}
