# Work to reconcile Tree_planting_report with Trees_removed_and_planted
planted %>% 
  filter(PlantYear==2018) %>% 
  summarize(Count=n(), Removed=sum(Removed))
# 2016: 400 planted, 53 removed
# 2018: 488 / 102

lost %>% 
  st_drop_geometry() %>% 
  filter(plant_year==2018) %>% 
  summarize(Count=n())
# 2016: 62
# 2018: 103

lost %>% 
  st_drop_geometry() %>% 
  filter(plant_year==2018, PlantDate < RemovalDate) %>% 
  summarize(Count=n())
# 2016: 53
# 2018: 102

lost_trees %>% 
  st_drop_geometry() %>% 
  filter(plant_year==2018, PlantDate < RemovalDate) %>% 
  summarize(Count=n())
# 2016: 52
# 2018: 99

# What is the difference?
missing_trees = setdiff(
  lost %>% filter(plant_year==2018, PlantDate < RemovalDate) %>% pull(TreeID),
  lost_trees %>% filter(plant_year==2018, PlantDate < RemovalDate) %>% pull(TreeID)
)

historical_ %>% 
  filter(TreeID %in% missing_trees) %>% 
  View()

# Count planting year - these are the same
# Tree_planting_report
trees1 %>%
  st_drop_geometry() %>% 
  count(year(Cartegra_1))

trees2 %>%
  st_drop_geometry() %>% 
  count(year(Cartegra_1))

# Look at removals
# Tree_planting_report - this is basically `planted`
trees1 %>% 
  st_drop_geometry() %>% 
  # Use Cartegra_1 for PlantDate in this data
  mutate(PlantDate=Cartegra_1) %>% 
  filter(!is.na(PlantDate)) %>% 
  mutate(PlantYear = year(PlantDate),
         Removed = (!is.na(RemovalDat) & RemovalDat > PlantDate)) %>% 
  group_by(PlantYear) %>% 
  summarise(Count=n(), Removed=sum(Removed))

# Trees_removed_and_planted
# This matches tree_planting_report
trees2 %>% 
  st_drop_geometry() %>% 
  filter(RemovalDate > Cartegra_1) %>%
  group_by(plant_year) %>% 
  summarize(Count=n())

# The difference between planted and lost is the trees with
# RemovalDate before PlantDate
trees2 %>% 
  st_drop_geometry() %>% 
  filter(removal_year >= 2016, plant_year==2016, PlantDate >= RemovalDate) %>% 
  nrow()
# 9
