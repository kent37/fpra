---
title: "Cambridge Tree Removal Permits"
author: "Kent S Johnson"
toc: true
callout-icon: false
format:
  html:
    embed-resources: true
    header-includes: '<link rel="icon" type="image/png" href="icons8-oak-tree-48.png">'
execute:
  echo: false
  message: false
  warning: false
---


<style>
table.gt_table {
    color: var(--quarto-body-color);
    font-size: 14px;
}
</style>

```{r libraries, include=FALSE}
library(tidyverse)
library(gt)
library(leaflet)
library(lubridate)
library(scales)
library(sf)
```

```{r data, include=FALSE}
permits_raw = 
  read_csv('/Users/kent/Dev/CambridgeTrees/Tree_Removal_Permits.csv',
           show_col_types=FALSE)

permits = permits_raw %>% 
  mutate(
    Number = replace_na(Number, 1),
    `Mitigation Fee` = replace_na(`Mitigation Fee`, 0),
    # Mitigation fee is not paid if there is a Reason to remove
    `Mitigation Fee` = if_else(is.na(Reason), `Mitigation Fee`, 0),
    Replacements = replace_na(Replacements, 0),
    `Issue Date` = mdy_hms(`Issue Date`),
    Year = year(`Issue Date`),
    FY = Year + if_else(month(`Issue Date`) >= 7, 1, 0),
    FY=paste0('FY', FY-2000),
    est_diameter = case_when(
      Diameter == 300 ~ 30, # Bad data??
      !is.na(Diameter) ~ Diameter,
      !is.na(`Exceptional Tree`) ~ 30,
      # Definition of significant tree changed in FY22
      TRUE ~ if_else(FY<=2021, 8, 6)
    ),
    est_total_diameter = est_diameter * Number
  )

# nbhd = st_read(here::here('Cambridge Open Data', 'Shapefiles', 'BOUNDARY_CDDNeighborhoods.shp/BOUNDARY_CDDNeighborhoods.shp'), quiet=TRUE) %>% 
#   st_transform(4326) %>% 
#   select(Neighborhood=NAME, geometry)

caption = md('Data: [City of Cambridge](https://data.cambridgema.gov/Public-Works/Tree-Removal-Permits/vj95-me7d/data) | Analysis: Kent S Johnson')
```

### Introduction

This report summarizes information about tree removal permits issued
by the City of Cambridge from `r min(permits$Year)` to `r max(permits$Year)`.

Data is from the 
[City Open Data Portal](https://data.cambridgema.gov/Public-Works/Tree-Removal-Permits/vj95-me7d/data).

### Reason for removal

In this period, `r comma(nrow(permits))` permits were issued. Of these,
`r comma(sum(!is.na(permits$Reason)))` 
listed a reason for removal of the tree,
exempting the removal from mitigation requirements. 
`r sum(is.na(permits$Reason))` permits do not list a reason and required
mitigation.^[This analysis may under-estimate the permits requiring mitigation. For permits to remove multiple trees, the reason may not apply to all trees in the permit.]

This table summarizes the reasons given:

```{r reasons}
permits |> 
  filter(!is.na(Reason)) |> 
  count(Reason, sort=TRUE, name='Number of permits') |> 
  mutate(Reason = str_remove(Reason, '\\d\\. ')) |> 
  gt(rowname_col='Reason') |> 
  tab_header(title='Reason given for tree removal') |> 
  tab_source_note(caption)
```

### Tree diameter

The diameter information is the public data is incomplete, especially for
FY22 and FY23. Trees with no diameter information were assigned a diameter
of 8" for permits in FY21 and before, and 6" after. This table shows the number of trees with and without diameter information:

```{r diameter}
permits |> 
  count(FY, !is.na(Diameter), is.na(Diameter)) |> 
  pivot_wider(names_from=c(2, 3), values_from=n) |> 
  select(c(1, 3, 2)) |> 
  gt(rowname_col='FY') |> 
  cols_label(FALSE_TRUE='No diameter given', TRUE_FALSE='Diameter given') |> 
  tab_header('Number of permits with diameter information') |> 
  tab_source_note(caption)
```


The total diameter shown in the next table 
is based on estimated total diameter as explained above.

### Summary of permits issued

```{r table1}
permits |> 
  summarize(.by=FY,
            `Permits issued` = n(),
            `Trees removed`=sum(Number),
            `Exceptional trees removed`=sum(est_diameter>=30),
            `Total diameter removed (est. in.)`=sum(est_total_diameter),
            `Replacements planted`=sum(Replacements),
            `Mitigation required` = sum(is.na(Reason)),
            `Total mitigation fees` = sum(`Mitigation Fee`)) |> 
  gt(rowname_col='FY') |> 
  #cols_label(FY='Fiscal year') |> 
  grand_summary_rows(colums=c(1, 2, 3, 5),
                     fns=list(id='Total', label='Total', 
                              fn=~scales::comma(round(sum(.), 0)))) |> 
  fmt_integer(columns=5) |> 
  fmt_currency(columns=8, decimals=0) |> 
  tab_header(title='Tree Permits Issued by Fiscal Year') |> 
  tab_source_note(caption)
```

### Top fees and removals

These tables show the top ten permits by mitigation fee and 
number of trees removed.

```{r tops}
details = permits |> 
  select(Address, Number, `Mitigation Fee`) |> 
  mutate(Address = str_remove(Address, ', Cambridge.*'))

details |> 
  slice_max(`Mitigation Fee`, n=10) |> 
  gt() |> 
  cols_label(Number='Number removed') |> 
  fmt_currency(columns='Mitigation Fee', decimals=0) |> 
  tab_header(title='Top ten permits by mitigation fee') |> 
  tab_source_note(caption)

details |> 
  slice_max(Number, n=10) |> 
  gt() |> 
  cols_label(Number='Number removed') |> 
  fmt_currency(columns='Mitigation Fee', decimals=0) |> 
  tab_header(title='Top ten permits by number of trees removed') |> 
  tab_source_note(caption)

```

### Locations

This map shows the locations of all permitted tree removals.
The circle diameters are proportional to the total diameter
of trees removed.

```{r map}
permits_to_map = permits |> 
  filter(!is.na(Longitude)) |> 
  st_as_sf(coords=c('Longitude', 'Latitude'), crs=4326) |> 
  mutate(short_address = str_remove(Address, ', Cambridge.*$'),
         Reason = str_remove(Reason, '^\\d\\. ') |> replace_na(''),
         `Reason Description` = replace_na(`Reason Description`, ''))

permits_to_map$popup=str_glue_data(permits_to_map, 
                  '{short_address}<br>',
                  '{Number} trees, ',
                  '{est_diameter} inches<br>',
                  '{Reason}<br>',
                  '{`Reason Description`}<br>')


leaflet(options=leafletOptions(maxZoom=20), width='98%', height='800px') %>% 
  addProviderTiles('CartoDB.Positron') %>% 
  setView(-71.117, 42.378, zoom = 14) %>% 
  addCircles(data=permits_to_map, 
             color='#d95f02',
             label=~short_address,
             popup=~popup,
             fillOpacity=0.6, stroke=FALSE,
             radius=~pmax(2,sqrt(est_total_diameter))*5)

```

<small>Copyright `r format(Sys.Date(), '%Y')` Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle;display:inline" 
  src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" target="_blank"/></a>
  <span style='float:right;font-style: italic;'>`r Sys.Date()`</span></small>
