# Explore historical tree data
# We want to see all planted / removed pairs, including
# those that were re-planted after removal

library(tidyverse)
library(sf)

source(here::here('Trees/ReadAndCleanTrees.R'))

history_files = '~/Dev/CambridgeTrees' %>% 
  list.dirs(recursive=FALSE)

history_dates = history_files %>% 
  str_extract('\\d{4}_\\d{2}[_-]\\d{2}') %>% 
  ymd()

# Get historical data for all trees
# Use TreeID as the key
historical_all = history_files %>%
  map_dfr(.id='file_date', function(path) {
    cat(basename(path), '\n')
    readAndCleanTrees(path) %>% 
      mutate(plant_year=year(PlantDate), 
             removal_year=year(RemovalDate))
  })

historical = historical_all %>% 
  st_drop_geometry()  %>% # More trouble than it's worth...
  filter(!duplicated(sf::st_drop_geometry(historical_all) %>% 
                       select(-file_date, -last_edi_1))) %>% 
  select(TreeID, file_date, everything()) %>% 
  mutate(file_date = history_dates[as.numeric(file_date)])

# Try to understand what history looks like
historical %>% 
  select(TreeID) %>% 
  count(TreeID, sort=T) %>% 
  head(40) -> top_treeid

historical %>% 
  filter(TreeID %in% top_treeid$TreeID) %>% 
  arrange(desc(TreeID), file_date) %>% 
  View()

date_counts = historical %>% 
  group_by(TreeID) %>% 
  summarize(NPlant=length(unique(discard(PlantDate, is.na))),
            NRemove=length(unique(discard(RemovalDate, is.na))))

# There do not appear to be many TreeIDs where a tree was planted,
# removed, and replanted. There are many instances of TreeIDs with
# multiple removal dates and no plant date.
date_counts %>% arrange(desc(NPlant)) %>% head()
date_counts %>% count(NPlant)
date_counts %>% count(NRemove)

