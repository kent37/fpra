# Helpers for Tree planting report

# Format as percent with no decimal
int_pct = function(val) scales::percent(val, accuracy=1)

# Make a table with all categories and yearly survival
make_year_table = function(planted, year) {
  table_data = planted %>% 
    filter(PlantYear==year) %>% 
    mutate(RemovalYear=if_else(Removed, str_glue('Year {ceiling(Time_to_removal/365)}'), 'Remaining')) %>% 
    mutate(RemovalYear=fct_drop(factor(RemovalYear, 
                              levels=c('Year 1', 'Year 2', 'Year 3', 'Year 4', 'Year 5', 'Year 6', 'Remaining'))) )
  
  big_table = bind_rows(
    make_tree_table(table_data, BareRoot) %>% 
      add_column(Category='Technique', .before=1),
    make_tree_table(table_data, `Diameter (in.)`) %>% 
      add_column(Category='Diameter', .before=1),
    make_tree_table(table_data, Company) %>% 
      add_column(Category='Company', .before=1),
    make_tree_table(table_data, Location) %>% 
      add_column(Category='Location', .before=1),
    make_summary_table(table_data)
  )
  big_table
}

# Format a year table using {gt}
format_year_table = function(big_table, year) {
  # Make a table
  big_table %>% 
    gt(groupname_col='Category', rowname_col='Description') %>% 
    # Split column names to create sub-heads
    tab_spanner_delim('_') %>% 
    tab_options(row_group.as_column = TRUE) %>% 
    tab_header(title=glue::glue('Cumulative yearly sapling removal, saplings planted in {year}')) %>% 
  tree_table_options()
}

# https://stackoverflow.com/questions/41862812/how-to-format-a-table-with-counts-percents-and-marginal-totals
# and {gt}
make_tree_table = function(table_data, group_column=NULL) {
  table_data %>% 
    # Get removal counts
    count(RemovalYear, {{group_column}}, name='Count') %>% 
   # filter(RemovalYear != 'Remaining') %>% 
    complete(RemovalYear, {{group_column}}, fill=list(Count=0)) %>% 
    # Compute cumulative percents and remove extra columns and rows
    group_by({{group_column}}) %>% 
    mutate(CumCount=cumsum(Count)) %>% 
    mutate(`Cumulative Percent Removed`=int_pct(CumCount/sum(Count))) %>% 
    select(-Count, -CumCount) %>% 
    filter(RemovalYear != 'Remaining') %>% 
    pivot_longer(cols=c(-RemovalYear, -{{group_column}})) %>% 
    # Add row totals
    bind_rows(table_data %>%
                count({{group_column}}, name='value') %>%
                mutate(value=as.character(value),
                       RemovalYear='Count', name='Total')) %>%
    # Long data with combined names
    unite(year_value, name, RemovalYear) %>% 
    # Wide data with combined names
    pivot_wider(names_from='year_value', values_from='value') %>% 
    # Clean up names and column order
    select(Description={{group_column}}, 
           Planted=Total_Count, 
           everything())
  }

make_summary_table = function(table_data) {
  table_data %>% 
    # Get removal counts
    count(RemovalYear, name='Count') %>% 
    # Add a total row
    #add_row(RemovalYear='Planted', Count=nrow(table_data), .before=1) %>% 
    # Compute percents
    mutate(CumCount=cumsum(Count)) %>% 
    mutate(CumPct=int_pct(CumCount/sum(Count))) %>%
    select(-Count, -CumCount) %>% 
    filter(RemovalYear != 'Remaining') %>% 
    # Wide data with combined names
    mutate(RemovalYear=paste0('Cumulative Percent Removed_', RemovalYear)) %>% 
    pivot_wider(names_from='RemovalYear', values_from='CumPct') %>% 
    # Clean up names and column order
    mutate(Category='Total', Description='Total Planted', 
           Planted=as.character(nrow(table_data))) %>% 
    select(Category, Description, Planted, 
           everything())
}

# Survival curves
library(survival)
library(survminer)

fit_survival_stratified = function(planted, strata) {
  # Use date of last removal as the date of last observation for
  # censored (surviving) data
  end_date = max(planted$RemovalDat, na.rm=TRUE) 
  plot_data = planted %>% 
    mutate(strata = factor(planted[[.env$strata]]),
           event = !is.na(Time_to_removal),
           Time_to_removal = if_else(is.na(Time_to_removal),
             as.double(end_date-PlantDate), 
             Time_to_removal),
           Months_to_removal = Time_to_removal/30)
  fit <- survfit(Surv(Months_to_removal, event) ~ strata,
               data = plot_data)
  list(fit=fit, plot_data=plot_data)
}

plot_survival_stratified = 
  function(fit, plot_data, strata_name, 
           conf.int=TRUE, title=NULL, ncensor.plot=FALSE,
           vline_at=NULL) 
{
  if (is.null(title)) title='Cambridge saplings planted 2016-2022'
  
  ps = ggsurvplot(fit, data = plot_data, 
             break.time.by = 6,
             conf.int=conf.int,
             palette=other_colors,
             censor.size=3,
             title=title,
             subtitle = glue::glue('Probability of non-removal stratified by {str_to_lower(strata_name)}'),
             caption=source_text,
             font.caption=c(10, 'plain', 'black'),
             legend.title=strata_name,
             legend.labs=levels(plot_data$strata),
             xlab='Time (approximate months)',
             ylab='Probability of remaining',
             ncensor.plot = ncensor.plot,      # plot the number of censored subjects at time t
             ncensor.plot.title='Number censored',
             )
  
  if (!is.null(vline_at))
    ps$plot = ps$plot +  geom_vline(xintercept=vline_at, 
               linetype=2, color='gray50')
  ps
}

fit_survival_all = function(planted) {
  # Use date of last removal as the date of last observation for
  # censored (surviving) data
  end_date = max(planted$RemovalDat, na.rm=TRUE) 
  plot_data = planted %>% 
    mutate(event = !is.na(Time_to_removal),
           Time_to_removal = if_else(is.na(Time_to_removal),
             as.double(end_date-PlantDate), 
             Time_to_removal),
           Months_to_removal = Time_to_removal/30)
  fit <- survfit(Surv(Months_to_removal, event) ~ 1,
               data = plot_data)
  list(fit=fit, plot_data=plot_data)
}

plot_survival_all = function(fit, plot_data) {
  suppressMessages(
  ggsurvplot(fit, data=plot_data,
             break.time.by = 6,
             conf.int=TRUE,
             censor.size=3,
             title='Cambridge saplings planted 2016-2022',
             subtitle = 'Overall probability of non-removal',
             legend.title='',
             caption=source_text,
             font.caption=c(10, 'plain', 'black'),
             xlab='Time (approximate months)',
             ylab='Probability of remaining',
             legend='none', 
             ncensor.plot=FALSE,
             #color='#1B9E77', # Needed if ncensor.plot = TRUE
             palette='#1B9E77', conf.int.fill='#1B9E77',
             ncensor.plot.title='Number censored'
  ))
}

make_survival_table = function(fit, strata_name, title, 
                               times=c(12, 36), sort=FALSE) {
  surv36 = summary(fit, times=times, extend=TRUE)
  table_data = surv36[c('strata', 'time', 'surv')] %>% 
    as_tibble() %>% 
    mutate(strata=forcats::fct_relabel(strata, ~str_remove(.x, 'strata='))) %>% 
    transmute({{strata_name}}:=strata, 
              Time=months_to_year_string(time), 
              Removed=int_pct(1-surv)) %>% 
    pivot_wider(names_from='Time', values_from='Removed')
  
  if (sort) table_data = table_data %>% 
    mutate(order=readr::parse_number(.[[ncol(.)]])) %>% 
    arrange(order) %>% 
    select(-order)
  
  table_data %>% 
    gt() %>% 
    tab_header(title=title) %>% 
    tree_table_options()
}

months_to_year_string = function(months) {
  case_when(months==12 ~ 'One year',
            months==36 ~ 'Three years',
            months==60 ~ 'Five year',
            TRUE ~ paste0(months/12, ' years'))
}

text_table = function(d) {
  knitr::kable(d, format='simple')
}