# Proportional hazards model of sapling failure

# Using `planted` from Tree_planting_report

# Make some predictor variables
end_date = max(planted$RemovalDat, na.rm=TRUE)
fit_data = planted %>%
  filter(!is.na(diameter)) %>% # Just a few of these
  select(PlantDate, PlantYear, RemovalDat, RemovalYear, Removed, Time_to_removal,
         diameter, Location, BareRoot, Company, CommonName,
         Ownership) %>%
  mutate(event = !is.na(Time_to_removal),
         Year=factor(as.character(PlantYear)), # Not continuous!!
         spring_planting=(month(PlantDate)<=8 & month(PlantDate) >=3),
         lt_2in=diameter<2,
         Time_to_removal = if_else(is.na(Time_to_removal),
           as.double(end_date-PlantDate),
           Time_to_removal),
         Months_to_removal = Time_to_removal/30) %>% 
  mutate(CommonName = case_when(
    str_detect(CommonName, 'elm|Elm') ~ 'Elm',
    str_detect(CommonName, 'Oak') ~ 'Oak',
    str_detect(CommonName, 'planetree|Planetree') ~ 'London Planetree',
    str_detect(CommonName, 'Serviceberry') ~ 'Serviceberry',
    str_detect(CommonName, 'hackberry') ~ 'Hackberry',
    TRUE ~ CommonName
  ))

# write_csv(fit_data, 'fit_data.csv')

# It's hard to know whether a flat recommendation to plant trees larger than 2"
# is the right conclusion to draw. Sapling size is confounded with both planting
# technique (bareroot trees are smaller) and species. There are big differences
# in sapling size by species, and the ones that are planted bigger are better
# survivors. Is it because they are larger or because the species is more
# robust?

fit_data = readr::read_csv('fit_data.csv')

# Predict via diameter, Year, BareRoot, spring_planting
library(survival)
fit = coxph(Surv(Months_to_removal, event) ~ 
              lt_2in + BareRoot, 
            data=fit_data, x=TRUE)

summary(fit)
cox.zph(fit)
plot(cox.zph(fit))

# Look at just Kentucky coffeetree and London planetree to try to sort out
# species vs size. They have similar proportions of > 2" trees and similar
# proportions of bareroot.
two_species = fit_data %>% 
  filter(!is.na(CommonName)) %>% 
  filter(CommonName %in% c('Kentucky Coffeetree', 'London Planetree'))

fit = coxph(Surv(Months_to_removal, event) ~ 
              lt_2in + CommonName + BareRoot, 
            data=two_species, x=TRUE)

summary(fit)
cox.zph(fit)
plot(cox.zph(fit))

fit_data2 = two_species %>% 
  mutate(strata=factor(paste(CommonName, BareRoot, lt_2in)))
fit2 = fit_data2 %>% 
  survfit(Surv(Months_to_removal, event) ~ strata,
               data = .)
plot_survival_stratified(fit2, fit_data2, 
                         'Species / Bareroot / Diameter', 
                         vline_at=c(12, 36), conf.int=FALSE)
  
# What about the top 10 species?
top_species_data = fit_data %>% 
  filter(!is.na(CommonName), CommonName %in% top_species)

fit = coxph(Surv(Months_to_removal, event) ~ 
              lt_2in + CommonName + BareRoot + spring_planting, 
            data=top_species_data, x=TRUE)

summary(fit)
cox.zph(fit)
plot(cox.zph(fit))

# Pretty much every way I slice it, I find that
# - Bigger is better. Saplings over 2" perform dramatically better than smaller 
#   saplings, even after taking planting technique and species into account.
# - Bareroot trees seem to perform slightly better than b&b when you take
#   sapling size into account (i.e. for similar sizes, bareroot does slightly better).
# - There are significant differences in survival by species, even after 
#   accounting for sapling size.
# - Fall plantings seem to do slightly better than spring

# Does the hazard ratio match up with the per-species survival?
# Pretty well except for Cherry which I think is the default so it would have
# coef==0 and exp(coef)==1, which is pretty far from Elm as the next smaller
# hazard
summary(fit)$coefficients %>% 
  as_tibble() %>% 
  add_column(CommonName=dimnames(summary(fit)$coefficients)[[1]], .before=1) %>% 
  arrange(`exp(coef)`)
