---
title: "Cambridge Street Trees - Tulips and copper beech"
author: "Kent Johnson"
output:
  html_vignette:
    css: ~/Google Drive/vignette.css
---

This map shows tulip trees (Liriodendron tulipifera) and European beech 
(Fagus sylvatica) from the 
[Cambridge street tree data](https://data.cambridgema.gov/Public-Works/Street-Trees/ni4i-5bnn).  

Large trees are shown as larger cicles. Use the layer menu at top right to select individual species. Click on a tree for details.

```{r parameters, echo=FALSE,include=FALSE,message=FALSE}
library(tidyverse)
library(forcats)
library(leaflet)
library(rgdal)
library(scales)
library(sf)
knitr::opts_chunk$set(echo=FALSE,fig.width=10, fig.height=8, comment=NA, warning=FALSE, message=FALSE)
```

```{r cache=TRUE}
source('ReadAndCleanTrees.R')
trees = readAndCleanTrees(
  '../Cambridge Open Data/ShapeFiles/ENVIRONMENTAL_StreetTrees.shp')
```

```{r}
# Re-project into OSM projection
trees_osm = st_transform(trees, 4326)

meters_per_foot = 0.3048
isTree = !is.na(trees$SiteType) & trees$SiteType=='Tree'
notTree = !is.na(trees$SiteType) & trees$SiteType!='Tree'

# Make a slippy map
map = leaflet(options=leafletOptions(maxZoom=20)) %>% 
  addProviderTiles('CartoDB.Positron') %>% 
  setView(-71.128184, 42.3769824, zoom = 14)

# Add a single tree species to the map
addTrees <- function(map, species_trees, specie, color) {
  popups = apply(species_trees, 1,
       function(tree) {
         paste0(
         '<p>Species: ', tree['SpeciesShort'], '<br>',
         ifelse(is.na(tree['Cultivar']), '', 
                paste0('Cultivar: ', tree['Cultivar'], '<br>')),
         'Condition: ', tree['TreeCondition'], '<br>',
         'Diameter: ', tree['diameter'], ' inch<br>',
         ifelse(is.na(tree['PlantDate']), '', 
                paste0('Planted: ', tree['PlantDate'], '<br>')),
         '</p>')
         })
  
  # Diameter is in inches, we need radius. 
  # Scale a bit bigger than true.
  map %>% addCircles(data=species_trees, color=color, group=specie,
                           fillOpacity=1, stroke=FALSE, popup=unname(popups), 
                         radius=pmax(10, species_trees$diameter*meters_per_foot),
                     label=specie) 
}

species = c('Liriodendron tulipifera', 'Fagus sylvatica')
colors = c('goldenrod', 'chocolate') %>% set_names(species)

# Actually add the species
for (specie in species)
{
  species_trees = trees_osm[!is.na(trees_osm$Scientific) 
                            & trees_osm$Scientific==specie & isTree,]
  map = addTrees(map, species_trees, specie, color=unname(colors[specie]))
}


# Finally the controls
map %>% addLayersControl(overlayGroups = species)

```

<small>&copy; `r format(Sys.Date(), '%Y')` Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle;display:inline" 
  src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" target="_blank"/></a>
  <span style='float:right;font-style: italic;'>`r Sys.Date()`</span></small>
