# Routing from local GraphHopper server
# Attributions:
# Powered by <a href="https://www.graphhopper.com/">GraphHopper API</a>
# Map data © OpenStreetMap contributors

library(httr)
library(glue)
meters_per_mile = 1609.34

url = 'http://localhost:8989/route?point=42.348934,-71.1604862&point=42.348903,-71.150358&debug=true&points_encoded=false&vehicle=bike&instructions=false'

get_route_gh = function(line) {
  coords = st_coordinates(line)
  debug='false'
  url = glue('http://localhost:8989/route?point={coords[1,2]}%2C{coords[1,1]}&point={coords[2,2]}%2C{coords[2,1]}&debug={debug}&points_encoded=false&vehicle=bike&instructions=false')
  r = GET(url)
  if (r$status_code != 200) {
    cat(line$start, '-', line$end, 'status:', r$status_code)
    return (no_route)
  }
  
  da = content(r, 'parsed')
  route1 = da[["paths"]][[1]]
  coords = route1$points$coordinates
  coords = do.call(rbind, map(coords, unlist))
  route = st_linestring(coords)
  duration = route1$time/1000/60 # Time is in mS
  distance = route1$distance / meters_per_mile
  data_frame(route=list(route), duration=duration, distance=distance)
}


get_time_gh = function(p1, p2) {
  url = glue('http://localhost:8989/route?point={p1[2]}%2C{p1[1]}&point={p2[2]}%2C{p2[1]}&vehicle=bike&calc_points=false')
  r = GET(url)
  if (r$status_code != 200) {
    cat(line$start, '-', line$end, 'status:', r$status_code)
    return (NA)
  }
  
  da = content(r, 'parsed')
  route1 = da[["paths"]][[1]]
  duration = route1$time/1000/60 # Time is in mS
  duration
}
