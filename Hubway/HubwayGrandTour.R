library(tidyverse)
library(mapview)
library(purrrlyr)
library(sf)
library(TSP)

d = read_csv('Hubway_Stations_as_of_July_2017.csv')

# Use NAD 1983 StatePlane Massachusetts Mainland FIPS 2001 Feet for
# proper distance measurements
d = st_as_sf(d, coords=c('Longitude', 'Latitude'), crs=4326) %>% 
  st_transform(crs="+proj=lcc +lat_1=41.71666666666667 +lat_2=42.68333333333333 +lat_0=41 +lon_0=-71.5 +x_0=199999.9999999999 +y_0=750000 +datum=NAD83 +units=us-ft +no_defs")

charlestown_ids = c('A32023', 'A32022', 'D32020', 'D32021', 'D32026', 'D32023')
eb_ids = c('A32034', 'A32035', 'A32028', 'A32027', 'A32032', 'A32036',
           'A32033', 'A32031', 'A32030', 'A32029')

camville = subset(d, Municipality %in% c('Cambridge', 'Somerville') | `Station ID` %in% charlestown_ids)

bos = subset(d, !Municipality %in% c('Cambridge', 'Somerville') &
               !`Station ID` %in% c(charlestown_ids, eb_ids))

eb = subset(d, `Station ID` %in% eb_ids)
# Let's get serious here

#concorde_path('/Users/kent/Dev/concorde/LINKERN')
# https://qmha.wordpress.com/2015/08/20/installing-concorde-on-mac-os-x/
concorde_path('/Users/kent/Dev/concorde/TSP')
crs = st_crs(d)

# Make a grand tour
# Returns an ordered list of station indexes
make_tour = function(stations) {
  pts = st_coordinates(stations)
  dst = dist(pts)
  tsp = TSP(dst, labels=stations$Station)
  
  tour = solve_TSP(tsp, method='concorde')
  tour
}

# Make shortest path from stations[,start] to stations[,end]
# start and end are station names
make_path = function(stations, start_name, end_name, time='straight') {
  m = switch(time,
    hubway = {
      # Route using Hubway time as metric
      n_stations = nrow(stations)
      m = matrix(0, nrow=n_stations, ncol=n_stations)
      for (i in seq_len(n_stations))
        for (j in seq_len(n_stations))
          m[i, j] = trip_times(stations$Station[i], stations$Station[j])$q5
      m[is.na(m)] = 10000
      m
    },
    straight={
      # Route using straight-line distance metric
      as.matrix(dist(st_coordinates(stations)))
    },
    gh = {
      n_stations = nrow(stations)
      m = matrix(0, nrow=n_stations, ncol=n_stations)
      geometry = st_geometry(stations) %>% st_transform(crs=4326)
      for (i in seq_len(n_stations))
        for (j in seq_len(n_stations))
          m[i, j] = get_time_gh(geometry[[i]], 
                               geometry[[j]])
      m
    })
  dimnames(m)=list(stations$Station, stations$Station)
  
  start <- which(stations$Station == start_name)
  end <- which(stations$Station == end_name)

  atsp <- ATSP(m[-c(start, end), -c(start, end)])
  atsp <- insert_dummy(atsp, label = "end/start")
  end_start <- which(labels(atsp) == "end/start")
  atsp[end_start, ] <- c(m[-c(start,end), start], 0)
  atsp[, end_start] <- c(m[end, -c(start,end)], 0) 
  
  #tour <- solve_TSP(atsp, method ="nearest_insertion", two_opt=TRUE)
  
  tsp <- reformulate_ATSP_as_TSP(atsp, cheap=0)
  tour = solve_TSP(tsp, method='concorde', precision=0)
  #tour2 <- solve_TSP(atsp, method ="two_opt", control = list(tour = tour))
  # tour = solve_TSP(tsp, method='linkern', control=list(
  #   exe='/Users/kent/Dev/concorde/LINKERN/linkern', 
  #   precision=0))
  tour <- as.TOUR(tour[tour <= n_of_cities(atsp)])
  
  labels = labels(cut_tour(tour, 'end/start'))
  path = match(labels, stations$Station)
  
  # The path might be in the reverse direction. We want path[1]
  # to be close to start
  if (m[path[1], start] > m[path[1], end]) path = rev(path)
  path = c(start, path, end)
  attr(path, 'length') = tour_length(tour, atsp)
  names(path) = stations$Station[path]
  path
}

show_path = function(st, path) show_tour(st, path, FALSE)

show_tour = function(st, path, loop=TRUE) {
  lines = make_lines(st, path, loop)
  show_lines(lines, st)
}

make_lines <- function(st, path, loop=TRUE) {
  st = st[path,]
  if (loop) {
    starts = st
    ends = rbind(starts[-1,], starts[1,])
  } else {
    starts = head(st, -1)
    ends = tail(st, -1)
  }
  
  lines = map(1:nrow(starts), function(i) {
    coords = st_coordinates(rbind(starts[i, 'geometry'], ends[i, 'geometry']))
    st_linestring(coords)
  }) %>% st_sfc(crs=crs)
  
  da = data_frame(start=starts$Station, end=ends$Station) %>% 
    mutate(time=map2(start, end, trip_times)) %>% unnest
  st_sf(da, lines)
}

show_lines <- function(da, st) {
  mapview(da, zcol='q5', na.color='red', color=viridisLite::magma) + mapview(st, cex=3, zcol='Station')
}
#tour = make_tour(camville)
#show_tour(tour)

start_name = "Packard Ave / Powderhouse Blvd"
end_name = "Warren St at Chelsea St"

cam_path = make_path(camville, start_name, end_name, time='gh')
show_path(camville, cam_path)

bos_start = "TD Garden - Causeway at Portal Park #2"
bos_end = "University of Massachusetts Boston - Integrated Sciences Complex"
bos_path = make_path(bos, bos_start, bos_end, time='gh')
show_path(bos, bos_path)

eb_start = 'Central Square - East Boston'
eb_end = 'Glendon St at Condor St'
eb_path = make_path(eb, eb_start, eb_end, time='gh')
show_path(eb, eb_path)

# Make a complete tour from parts
# We have to break the camville path at Sullivan Square and insert the
# East Boston path
cam_break1 = 'Broadway St at Mt Pleasant St'
cam_ix1 = which(names(cam_path)==cam_break1)
cam_break2 = 'Edwards Playground - Main St at Eden St'
cam_ix2 = which(names(cam_path)==cam_break2)

tour_names = c(
  names(cam_path)[1:cam_ix1],
  names(eb_path),
  names(cam_path)[cam_ix2:length(cam_path)],
  names(bos_path))

tour_path = match(tour_names, d$Station)
save(tour_path, file='GrandTourCamBosGHTimes.RData')

tour_lines = make_lines(d, tour_path, loop=FALSE)
show_lines(tour_lines, d)

# What if we just do everything?
# It doesn't route correctly
grand_tour = make_path(d, start_name, bos_end)
show_path(d, grand_tour)
save(grand_tour, file='GrandTourHubwayTimes.RData')
# load('GrandTourHubwayTimes.RData')
# Total distance
pts = st_coordinates(d)
dst = dist(pts)
tsp = TSP(dst, labels=stations$Station)
TSP:::tour_length.TOUR(tour_path, tsp)/5280 #77.72

# Time
tour_lines %>% as_data_frame %>% select(starts_with('q')) %>% 
  colSums(na.rm=TRUE)/60
#    5%    50%    95% 
# 10.21  22.41 181.63 

# Routing with Mapbox
routing_lines = tour_lines %>% st_transform(crs=4326)
routes = map_dfr(st_geometry(routing_lines), ~{
  Sys.sleep(1) # Mapbox allows 60 requests / second
  get_route_mapbox(.)
  })

to_eb = which(tour_lines$start=='Broadway St at Mt Pleasant St')
from_eb = which(tour_lines$start=='Glendon St at Condor St')

good_routes = tour_lines %>% as_data_frame %>% 
  select(start, end) %>% 
  bind_cols(routes %>% select(-route)) %>% 
  mutate(duration = round(duration/60, 1), distance=round(distance, 1))

st_geometry(good_routes) = st_sfc(routes$route, crs=4326)
good_routes[to_eb, c('duration', 'distance', 'geometry')] = eb1
good_routes[from_eb, c('duration', 'distance', 'geometry')] = eb2
mapview(good_routes, zcol='duration', 
        highlight=highlightOptions(color='red', weight=4)) + mapview(d, cex=3, zcol='Station')
# HubwayGrandTourMB.html

st_write(good_routes, 'HubwayTourMapboxRouting.gpkg')

sum(good_routes$duration) / 60 # 12.49 hours
sum(good_routes$distance) # 98 miles

# Routing with GraphHopper
routing_lines = tour_lines %>% st_transform(crs=4326)
routes = map_dfr(st_geometry(routing_lines), get_route_gh)

good_routes = tour_lines %>% as_data_frame %>% 
  select(start, end) %>% 
  bind_cols(routes %>% select(-route)) %>% 
  mutate(duration = round(duration, 1), distance=round(distance, 1))

st_geometry(good_routes) = st_sfc(routes$route, crs=4326)
mapview(good_routes, zcol='duration', 
        highlight=highlightOptions(color='red', weight=4)) + mapview(d, cex=3, zcol='Station')
st_write(good_routes, 'HubwayTourCamBosGHRouting.gpkg')

sum(good_routes$duration) / 60 # 9.96 hours
sum(good_routes$distance) # 103.7 miles

# Grand tour with GraphHopper times
# Try just leaving out East Boston
start_name = "Packard Ave / Powderhouse Blvd"
bos_end = "University of Massachusetts Boston - Integrated Sciences Complex"
no_eb = rbind(camville, bos)
no_eb_path = make_path(no_eb, start_name, bos_end, time='gh')
show_path(no_eb, no_eb_path)

eb_start = 'Central Square - East Boston'
eb_end = 'Glendon St at Condor St'
eb_path = make_path(eb, eb_start, eb_end, time='gh')
show_path(eb, eb_path)

cam_break1 = 'Broadway St at Mt Pleasant St'
cam_ix1 = which(names(no_eb_path)==cam_break1)
cam_break2 = 'Edwards Playground - Main St at Eden St'
cam_ix2 = which(names(no_eb_path)==cam_break2)

tour_names = c(
  names(no_eb_path)[1:cam_ix1],
  names(eb_path),
  names(no_eb_path)[cam_ix2:length(no_eb_path)])

tour_path = match(tour_names, d$Station)
tour_lines = make_lines(d, tour_path, loop=FALSE)
show_lines(tour_lines, d)

# Routing with GraphHopper
routing_lines = tour_lines %>% st_transform(crs=4326)
routes = map_dfr(st_geometry(routing_lines), get_route_gh)

to_eb = which(tour_lines$start=='Broadway St at Mt Pleasant St')
from_eb = which(tour_lines$start=='Glendon St at Condor St')

good_routes = tour_lines %>% as_data_frame %>% 
  select(start, end) %>% 
  bind_cols(routes %>% select(-route)) %>% 
  mutate(duration = round(duration, 1), distance=round(distance, 1))

st_geometry(good_routes) = st_sfc(routes$route, crs=4326)
good_routes[to_eb, c('duration', 'distance', 'geometry')] = eb1
good_routes[from_eb, c('duration', 'distance', 'geometry')] = eb2
mapview(good_routes, zcol='duration', 
        highlight=highlightOptions(color='red', weight=4)) + mapview(d, cex=3, zcol='Station')

st_write(good_routes, 'HubwayTourGHRouting.gpkg')
# HubwayGrandTourGH.html

sum(good_routes$duration) / 60 # 10.12 hours
sum(good_routes$distance) # 103.8 miles

# Compare Mapbox routes of the point-to-point routing
# with the GraphHopper routes
good_routes_mb = st_read('HubwayTourMapboxRouting.gpkg')
good_routes_gh = st_read('HubwayTourGHRouting.gpkg')

mapview(good_routes_mb, zcol='duration', color='darkred',
        highlight=highlightOptions(color='red', weight=4),
        layer.name='Mapbox', homebutton=FALSE) + 
  mapview(good_routes_gh, zcol='duration', color='darkgreen',
        highlight=highlightOptions(color='green', weight=4),
        layer.name='GraphHopper', homebutton=FALSE) + 
  mapview(d, cex=3, zcol='Station', layer.name='Stations', color='darkblue', homebutton=FALSE)
# HubwayGrandTourMapboxHG.html