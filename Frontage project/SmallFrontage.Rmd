---
title: "Cambridge Parcels with less than 20 ft frontage"
author: "Kent Johnson"
date: '`r Sys.Date()`'
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE}
library(dplyr)
library(DT)
library(leaflet)
library(pander)
library(rgdal)
library(rgeos)
library(sp)
library(stringr)
source('../Cambridge Open Data/CODutil.R')

knitr::opts_chunk$set(echo=FALSE,fig.width=10, fig.height=8, 
                      comment=NA, warning=FALSE, message=FALSE)
panderOptions("table.split.table", Inf)

parcels_with_frontage = readOGR('/Users/kent/Google Drive/Frontage project', 
        'parcels_with_frontage', stringsAsFactors=FALSE, verbose=FALSE)

small_parcels = parcels_with_frontage[parcels_with_frontage$frontage<20,]

ass2015 = read.csv('/Users/kent/Google Drive/FPRA/Cambridge Open Data/Assessing_Building_Information_clean_merged.csv', 
               stringsAsFactors=FALSE)

ass_data = left_join(small_parcels@data, ass2015 %>% group_by(GIS.ID) %>% filter(row_number()==1), by=c('ML'='GIS.ID'))
ass_data$frontage = round(ass_data$frontage, 1)

# Leave out townhouses and row houses, they are exempt from the frontage requirement
omit = c('ROW', 'ROW-END', 'TOWNHOUSE', 'TOWNHSE-END')
keep = !ass_data$Style.Desc %in% omit
small_parcels = small_parcels[which(keep),]
ass_data = ass_data[keep,]
```

This map reflects an attempt to find land parcels in Cambridge with less than 20' road frontage. It is not entirely accurate; in particular it includes several parcels with more than 20' frontage and it may omit some parcels with less than 20' frontage. The map shows `r nrow(small_parcels)` parcels. 

Methodology: 

- The Cambridge GIS [roads](http://www.cambridgema.gov/GIS/gisdatadictionary/Basemap/BASEMAP_Roads) and [sidewalks](http://www.cambridgema.gov/GIS/gisdatadictionary/Basemap/BASEMAP_Sidewalks) layers were combined and buffered by 10 feet. 
- The Cambridge [2015 parcel layer](http://www.cambridgema.gov/GIS/gisdatadictionary/Assessing/FY2015/ASSESSING_ParcelsFY2015) was split into individual segments.
- Parcel segments fully contained within the road & sidewalk buffer were considered frontage segments.
- The frontage segment lengths for each map-lot were added to get the total frontage for each lot.
- Property type was determined from the [2015 assessor's database](https://data.cambridgema.gov/Assessing/Assessing-Building-Information/crnm-mw9n).

Townhouses and row houses are excluded because they are exempt from the frontage requirement.

All property types are included; the breakdown is

```{r}
ass_data$Building.Type[is.na(ass_data$Building.Type)] = 'Other'
pander(table(ass_data$Building.Type))

popups = apply(ass_data, 1,
       function(unit1) {
         ml = unit1['ML']
         paste0(
         '<p>Map-Lot: <a href="http://www.cambridgema.gov/propertydatabase/', 
         str_trim(unit1['pid']), '" target="_blank">' , ml, '</a><br>',
         unit1['Location'], '<br>',
         'Building type: ', unit1['Building.Type'], '<br>',
         'Style: ', unit1['Style.Desc'], '<br>',
         'Frontage: ', unit1['frontage'], ' ft',
         '</p>')
         })

palette = c('Comm Condo'='#A31F34', Commercial='#A31F34', 
            'CondoUnit'='#006000', 'Indust/Comm'='#A31F34', Other='darkslategrey',
            Residential='#006000', 'Three Family'='#80A000', 'Two Family'='#80A000'
            )
colorKey(palette)

map = leaflet() %>% 
  addProviderTiles('CartoDB.Positron') %>% 
  setView(-71.128184, 42.3769824, zoom = 14)

# Re-project into OSM projection
parcels_osm = spTransform(small_parcels, CRS("+init=EPSG:4326"))
colors = unname(palette[ass_data$Building.Type])
map %>% addPolygons(data=parcels_osm, 
                    color=colors, fillColor=colors, 
                    opacity=0.7, fillOpacity=0.5,
                    weight=1, dashArray=1,           # Line weight
                    popup=unname(popups), 
                    options=c(pathOptions(), popupOptions(minWidth=100)))
```

Here is the complete list of properties shown:

```{r}
to_show = ass_data %>%
  mutate(`Map-Lot`=paste0('<a href="http://www.cambridgema.gov/propertydatabase/', 
         pid, '" target="_blank">' , ML, '</a>'), Frontage=frontage) %>% 
  select(`Map-Lot`, Location, Building.Type, Frontage) 

datatable(to_show, to_show, escape=FALSE, filter='top')
```

