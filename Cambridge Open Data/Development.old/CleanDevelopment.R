# Clean and geocode the full development log

library(ggplot2)
library(dplyr)
library(magrittr)

hist <- read.csv("~/Google Drive/FPRA/Cambridge Open Data/Development/Development_Log_Historical_Projects.csv")
hist = filter(hist, Year.Complete>=2010)
hist %<>% rename(Developer=Developer.Name)

current <- read.csv("~/Google Drive/FPRA/Cambridge Open Data/Development/Development_Log_Current_Edition_12_2014.csv")
common = intersect(names(hist), names(current))

# All except 1796 units in the North Point Remaining Master Plan
#all = rbind(hist[,common], subset(current, Project.Type!='Master Plan',select=common))
all = rbind(hist[,common], subset(current, select=common))

all$Project.Stage = factor(all$Project.Stage, 
                               levels=c('Permitting', 'Permit Granted or As of Right', 'Building Permit Granted', 'Complete'))
all$Year.Complete[is.na(all$Year.Complete)] = 'Future'
all = arrange(all, ProjectID)

# How many addresses are in the master address list?
library(rgdal)
addr = readOGR('../ADDRESS_AddressPoints.shp', 
                    'ADDRESS_AddressPoints',
                    stringsAsFactors=FALSE, verbose=FALSE)

sum(all$Address %in% addr$Full_Addr) # 3 :-/

# What are all the address suffixes in addr?
library(stringr)
suffixes = sapply(str_split(addr@data$Full_Addr, ' '), tail, 1)
suffixes = sort(unique(suffixes))
names(suffixes) = c("Avenue", 'NA', "Boulevard", 'NA', "Circle", 'NA', "Court", "Center", 
"Drive", 'NA', 'NA', "Gardens", 'NA', 'NA', "Highway", "Lane", "Landing", 
'NA', 'NA', "Park", "Parkway", "Place", "Road", "Square", "Street", "Terrace", 'NA', 
"Turnpike", 'NA', 'NA', 'NA', 'NA')
suffixes = suffixes[names(suffixes)!='NA']

suffixes = structure(c("Ave", "Blvd", "Cir", "Ct", "Ctr", "Dr", "Gdns", 
"Hwy", "Ln", "Lndg", "Pk", "Pkwy", "Pl", "Rd", "Sq", "St", "Ter", 
"Tpke"), .Names = c("Avenue", "Boulevard", "Circle", "Court", 
"Center", "Drive", "Gardens", "Highway", "Lane", "Landing", "Park", 
"Parkway", "Place", "Road",
"Square", "Street", "Terrace", "Turnpike"
))

# Brute force replace
devAddr = all$Address
for (suf in names(suffixes))
{
  rx = paste0(suf, '$')
  devAddr = str_replace(devAddr, rx, suffixes[suf])
}
sum(devAddr %in% addr@data$Full_Addr)
devAddr[!devAddr %in% addr@data$Full_Addr]

# Merge what we have and save a file for hand-editing
temp = data.frame(id=all$ProjectID, addr=devAddr)
temp = merge(temp, subset(addr@data, select=c('Full_Addr', 'ml')), by.x='addr', by.y='Full_Addr', all.x=TRUE, sort=FALSE)
temp = arrange(temp, id)
# Not sure why some rows are duplicated...
temp = temp[!duplicated(temp$id),]
all$GIS.ID = temp$ml
write.csv(all, 'Development_Log_Combined.csv', row.names=FALSE)
