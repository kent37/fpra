This folder contains archived copies of the yearly assessment data provided
to me by the Cambridge GIS Department. My understanding is that prior to the 
existance of the open data portal, these files were approved for public 
distribution on request.