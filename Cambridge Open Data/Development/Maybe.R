# Format helpers for map popups
library(htmltools)

# Format an optional item
maybe = function(item, head=NULL, brk=TRUE)
  if(is.null(item) || is.na(item) || (is.numeric(item) && item==0)) '' else tagList((if(brk) br()), head, item)

# Format a special permit (PB) link
maybe_sp = function(sp, head='Special Permit:', brk=TRUE) {
  if(is.null(sp) || is.na(sp) || (is.numeric(sp) && sp==0)) '' else  {
    tagList((if(brk) br()), head, 
             a(href=paste0("https://www.cambridgema.gov/specialpermits/", sp),
               target='_blank', sp))
  }
}

# Format a building permit link
maybe_bp = function(bp, head='Permit:', brk=TRUE) {
  if(is.null(bp) || is.na(bp) || (is.numeric(bp) && bp==0)) '' else  {
    tagList((if(brk) br()), head, 
             a(href=paste0("https://permits.cambridgema.gov/CAPSite/Permit/View/ByPermitNumber/", bp),
               target='_blank', bp))
  }
}

# Format a map-lot link
maybe_ml = function(ml, head='Map-Lot:', brk=TRUE) {
  if(is.null(ml) || is.na(ml)) '' else {
    tagList((if(brk) br()), head, 
             a(href=paste0("http://www.cambridgema.gov/propertydatabase/", ml),
               target='_blank', ml))
  }
}
