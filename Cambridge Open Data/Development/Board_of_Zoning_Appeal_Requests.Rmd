---
title: "Cambridge Board of Zoning Appeals Requests 2015-2017"
output: 
  flexdashboard::flex_dashboard:
    orientation: columns
    vertical_layout: fill
    source_code: embed
    theme: spacelab
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE}
library(dplyr)
library(htmltools)
library(leaflet)
library(lubridate)
#library(raster) # Don't use as library, it breaks dplyr::select
library(readr)
library(sp)
library(stringr)

source('../CODutil.R')
knitr::opts_chunk$set(echo=FALSE, fig.width=10, fig.height=6, results='asis',
                      comment=NA, warning=FALSE, message=FALSE)
```

2017
==========

Column {data-width=1000}
------------------

### Cambridge Board of Zoning Appeals Requests, 2017

```{r}
city = raster::shapefile('../BOUNDARY_CityBoundary.shp/BOUNDARY_CityBoundary.shp') %>% 
  spTransform(CRS("+init=EPSG:4326"))
bza = read_csv('../Board_of_Zoning_Appeal_Requests.csv') %>% 
    mutate(Year = year(mdy(`Application Date`, tz='America/New_York'))) %>% 
  filter(Year >= 2015) %>% parse_location

maybe = function(item, head=NULL, brk=TRUE)
  if(is.null(item) || is.na(item) || (is.numeric(item) && item==0)) '' else tagList((if(brk) br()), head, item)

make_popups = function(d)
  lapply(1:nrow(d),
       function(i) {
         row = d[i,]
         HTML(as.character(p(
           row$`Application Number`, br(),
           row$Location, br(), 
           row$`Type of Request`, br(),
           row$`Status of Request`, br(),
           maybe(row$`Summary For Publication`, brk=FALSE)
         )))
         })

bza$popups = make_popups(bza)
bza$labels = paste(bza$`Type of Request`, bza$`Status of Request`, sep=', ')
color_fun = colorFactor(c('blue', 'red', 'purple'), unique(bza$`Type of Request`))

map = leaflet() %>% addProviderTiles('Esri.WorldTopoMap') %>% 
  setView(-71.1129096, 42.3783904, zoom = 14) %>% 
  addPolygons(data=city, fill=FALSE, color='steelblue', weight=3)

map %>% addCircleMarkers(data=bza[bza$Year==2017,], color=~color_fun(`Type of Request`),
                         radius=5, stroke=FALSE, opacity=0.6, fillOpacity=0.6,
            label=~labels, popup=~popups, group=~`Status of Request`) %>% 
  addLayersControl(overlayGroups=sort(unique(bza$`Status of Request`))) %>% 
  addLegend(position='bottomleft', pal=color_fun, values=bza$`Type of Request`)

```

2016
==========

Column {data-width=1000}
------------------

### Cambridge Board of Zoning Appeals Requests, 2016

```{r}

map %>% addCircleMarkers(data=bza[bza$Year==2016,], color=~color_fun(`Type of Request`),
                         radius=5, stroke=FALSE, opacity=0.6, fillOpacity=0.6,
            label=~`Type of Request`, popup=~popups, group=~`Status of Request`) %>% 
  addLayersControl(overlayGroups=sort(unique(bza$`Status of Request`))) %>% 
  addLegend(position='bottomleft', pal=color_fun, values=bza$`Type of Request`)

```


2015
==========

Column {data-width=1000}
------------------

### Cambridge Board of Zoning Appeals Requests, 2015

```{r}

map %>% addCircleMarkers(data=bza[bza$Year==2015,], color=~color_fun(`Type of Request`),
                         radius=5, stroke=FALSE, opacity=0.6, fillOpacity=0.6,
            label=~`Type of Request`, popup=~popups, group=~`Status of Request`) %>% 
  addLayersControl(overlayGroups=sort(unique(bza$`Status of Request`))) %>% 
  addLegend(position='bottomleft', pal=color_fun, values=bza$`Type of Request`)
```


About
================

### About these maps

These maps show Cambridge variances and special permits with status of 
under review, approved, denied or withdrawn and application dates in 2015-2017.

Data is from the [City of Cambridge](http://cambridgema.gov) [Board of Zoning Appeal Requests](https://data.cambridgema.gov/Inspectional-Services/Board-of-Zoning-Appeal-Requests/urfm-usws), mapped by Kent S Johnson. 



<small>Copyright 2017 Kent S Johnson
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
  <img alt="Creative Commons License" style="border-width:0;vertical-align:middle;display:inline" 
  src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" target="_blank"/></a>
  <span style='float:right;font-style: italic;'>`r Sys.Date()`</span></small>
  