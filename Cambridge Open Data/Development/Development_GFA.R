library(tidyverse)

current = read_csv(here::here('Cambridge Open Data/Development/Development_Log_Current_Edition.csv'))

hist = read_csv(here::here('Cambridge Open Data/Development/Development_Log_Historical_Projects_1997_-_2017.csv'))

all = current %>% select(`Year Complete`, `Total GFA`, `Primary Use`) %>% 
  bind_rows(hist %>% select(`Year Complete`, `Total GFA`, `Primary Use`))

total = all %>% group_by(`Year Complete`) %>% 
  summarize(Count=n(), `Total GFA`=sum(`Total GFA`)) %>% 
  print(n=Inf)

total %>% mutate(`Year Complete`=as.character(`Year Complete`)) %>%
  bind_rows(total %>% summarise(Count=sum(Count), `Total GFA`=sum(`Total GFA`), `Year Complete`='Total')) %>% 
  write_csv(here::here('Cambridge Open Data/Development/Development_GFA.csv'))

total$`Year Complete`[is.na(total$`Year Complete`)] = 2019
ggplot(aes(`Year Complete`, `Total GFA`), data=total) +
  geom_col() + 
  scale_y_continuous(labels=scales::comma) +
  labs(x='Year Complete', y='Total Gross Floor Area (sq ft)',
       title='Yearly Gross Floor Area from Cambridge Development Log') +
  theme_minimal()

ggplot(aes(`Year Complete`, `Count`), data=total) +
  geom_col() + 
  scale_y_continuous(labels=scales::comma) +
  labs(x='Year Complete', y='Number of projects',
       title='Yearly Project Count from Cambridge Development Log') +
  theme_minimal()

current %>% filter(is.na(`Year Complete`)) %>% View
