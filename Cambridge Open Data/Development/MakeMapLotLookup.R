# Goal: A map-lot reference for properties in the historical log which are
# not in the City map-lot file. This file can be merged later with the historical log
# for mapping.

#library(raster) # Load before dplyr
library(dplyr)
library(readr)
library(stringr)

hist = read_csv('Development_Log_Historical_Projects_1997_-_2015.csv', 
                    na=c('', 'N/A')) %>% 
  rename('Developer'=`Developer Name`)
ml = read_csv('Development_Log_MapLots_2011_to_Present.csv', na=c('', 'N/A')) %>% 
  select(-Status, -LatLong)
hist = left_join(hist, ml)
hist_ml = hist %>% filter(!is.na(MapLot))
hist_no_ml = hist %>% filter(is.na(MapLot)) %>% select(-MapLot)

# Master address file
addr = raster::shapefile('../ADDRESS_AddressPoints.shp/ADDRESS_AddressPoints.shp')

# Lookup table to convert hist address format to addr format
suffixes = structure(c("Ave", "Blvd", "Cir", "Ct", "Ctr", "Dr", "Gdns", 
"Hwy", "Ln", "Lndg", "Pk", "Pkwy", "Pl", "Rd", "Sq", "St", "Ter", 
"Tpke"), .Names = c("Avenue", "Boulevard", "Circle", "Court", 
"Center", "Drive", "Gardens", "Highway", "Lane", "Landing", "Park", 
"Parkway", "Place", "Road",
"Square", "Street", "Terrace", "Turnpike"
))

# Brute force replace
devAddr = hist_no_ml$Address
for (suf in names(suffixes))
{
  rx = paste0(suf, '$')
  devAddr = str_replace(devAddr, rx, suffixes[suf])
}

sum(devAddr %in% addr@data$Full_Addr)
devAddr[!devAddr %in% addr@data$Full_Addr]

# Merge what we have and save a file for hand-editing
temp = data.frame(ProjectID=hist_no_ml$ProjectID, addr=devAddr)
temp = merge(temp, 
             (addr@data %>% select(Full_Addr, ml) %>% rename(MapLot=ml)), 
             by.x='addr', by.y='Full_Addr')

# Some rows are duplicated because there are multiple entries in addr
temp = temp[!duplicated(temp$ProjectID),]
hist_no_ml = hist_no_ml %>% left_join(temp %>% select(-addr))
hist = rbind(hist_ml, hist_no_ml)
write_csv(hist, 'Historical_Projects_MapLot.csv')
