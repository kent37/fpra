State codes from various sources:
- Cambridge Property Classes.xslx, .csv - This is straight from the HTML source of the assessor's property search page
- StateClassification.tsv - the official state codes from the state code book, augmented with Cambridge codes from the above where the Cambridge codes are additional to the state codes
- classificationcodebook.pdf - the state code book
- State Use Codes for Database.xls and State Use Codes.xls are two versions of the same codes from Jeff Amero. 
- State Use Codes for Database.csv is State Use Codes for Database.xls augmented with missing codes from StateClassification.tsv and Use Code Desc.txt
- Use Code Desc.xslx, .txt - From Andrew Johnson at the city assessors office