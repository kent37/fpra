# Join the assessor's database with the parcel shapefiles to add zoning information

library(rgdal)
library(rgeos)

ass2015 = read.csv('Assessing_Building_Information_clean_merged.csv', stringsAsFactors=FALSE)
parcels2015 = readOGR('ASSESSING_ParcelsFY2015.shp/', 'ASSESSING_ParcelsFY2015', stringsAsFactors=FALSE)
zoning2015 = readOGR('CDD_ZoningDistricts.shp', 'CDD_ZoningDistricts', stringsAsFactors=FALSE)

# How well do they match up?
length(unique(ass2015$GIS.ID)) # 12962
length(unique(parcels2015@data$ML)) # 13005

sum(!(unique(ass2015$GIS.ID) %in% unique(parcels2015@data$ML))) # 12
sum(!(unique(parcels2015@data$ML) %in% unique(ass2015$GIS.ID))) # 55

# Most of the differences are extra parcels. We want to work with the assessors data
# so we can't do much with them. Nor can we really do anything with the 12 parcels not in
# the map. So just subset to the common items

parcels2015 = parcels2015[parcels2015@data$ML %in% ass2015$GIS.ID,]
ass2015 = subset(ass2015, GIS.ID %in% parcels2015@data$ML)

# Now they have the same parcels
sum(!(unique(ass2015$GIS.ID) %in% unique(parcels2015@data$ML))) # 0
sum(!(unique(parcels2015@data$ML) %in% unique(ass2015$GIS.ID))) # 0

# Find the zoning district of each parcel
# First cut just takes the first match
z = over(parcels2015, zoning2015)
parcels2015$ZONE_TYPE = zoning2015$ZONE_TYPE[z]

# Now deal with the multiple hits
z=over(parcels2015, zoning2015, returnList=TRUE)
table(sapply(z, length)) # Lots of multiple hits ...

# For parcels in multiple districts, this finds the majority district
# From http://stackoverflow.com/questions/14208016/find-best-matching-overlapping-polygons-in-r
for (i in 1:length(z)){
    tmp <- length(z[[i]])
    if (tmp>1){
        areas <- numeric(tmp)
        for (j in 1:tmp)
            areas[j] <- gArea(gIntersection(parcels2015[i,], zoning2015[z[[i]][j],]))
       parcels2015$ZONE_TYPE[i] <- zoning2015$ZONE_TYPE[z[[i]][which(areas==max(areas))]]
    }
}

plot(parcels2015, col=as.factor(parcels2015$ZONE_TYPE))
writeOGR(parcels2015, 'ASSESSING_ParcelsFY2015_w_zone.shp/', 'ASSESSING_ParcelsFY2015', driver='ESRI Shapefile')

# Merge the zone into the assessors data
a=merge(ass2015, parcels2015@data[,c('ML', 'ZONE_TYPE')], by.x='GIS.ID', by.y='ML')

# Three rows are added!? Merge shouldn't do that...
a = a[!duplicated(a),]

# Put the columns in a more convenient order
n = names(a)
interesting = c('Account.Number', 'GIS.ID', 'pid', 'State.Use', 'Building.Type', 'Location', 'Building.No', 'Owner.Name', 'Owner.s.Name2',
                'Mailing.Address', 'City', 'State', 'Zip', 'bldg_name')
a = a[,c(interesting, n[!n %in% interesting])]
write.csv(a, 'Assessing_Building_Information_clean_merged_w_zone.csv', row.names=FALSE)

