# Coding for the FCODE field of the Open Space layer, from Cambridge GIS via Josh Wolff:
1
City park or playground
3
Public school grounds
5
Cemetery
6
Urban plaza
7
Traffic island
8
National park site
11
Municipal golf course
12
Park under development
21
DCR-owned park
22; 24; 27
DCR-owned parkway
39
Former school location
41
Privately owned, publicly accessible park
42
Privately owned, publicly accessible plaza
43
Publicly accessible rooftop garden
49
Private school sports field
99
Other