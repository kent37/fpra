---
title: "Parcels built since 1980"
author: "Kent Johnson"
date: '`r Sys.Date()`'
output:
  html_document
---

This document shows parcels in the assessor's database where the "Actual Year Built"
is 1980 or later. This map shows the parcels with new units. Darker colors are more recent construction.

```{r parameters, echo=FALSE,include=FALSE,message=FALSE}
library(leaflet)
library(rleafmap)
library(rgdal)
knitr::opts_chunk$set(echo=FALSE,fig.width=10, fig.height=8, comment=NA, warning=FALSE, message=FALSE)
options(width=100)
```


```{r}
ass2015 = read.csv('../Cambridge Open Data/Assessing_Building_Information.csv', 
               stringsAsFactors=FALSE)

parcels2015 = readOGR('../Cambridge Open Data/ASSESSING_ParcelsFY2015.shp', 'ASSESSING_ParcelsFY2015',
                      stringsAsFactors=FALSE, verbose=FALSE)

# Assessed properties; just one property per parcel
el = subset(ass2015, Actual.Year.Built>=1980) 
el = el[!duplicated(el$GIS.ID),]

# Get the actual parcels and merge
el_parcels = parcels2015[parcels2015$ML %in% el$GIS.ID,]
el_parcels = merge(el_parcels, el, by.x='ML', by.y='GIS.ID')

# Make popups showing the location and year built.

popups = sapply(el_parcels@data, 
               function(parcel) {
                 paste0('<p>', parcel['Location'], '<br>',
                 'Year: ', parcel['Actual.Year.Built'], '<br></p>')
                 })

palette = c('#fed976', '#feb24c', '#fd8d3c', '#fc4e2a', '#e31a1c', '#bd0026', '#800026')

year_built = el_parcels@data$Actual.Year.Built
colors = palette[cut(year_built, c(seq(1980, 2010, 5), 2015), include.lowest=TRUE, labels=FALSE)]

# Make a slippy map

# Re-project into OSM projection
el_osm = spTransform(el_parcels, CRS("+init=EPSG:4326"))

leaflet() %>% 
  addProviderTiles('Stamen.TonerLite') %>% 
  setView(-71.128184, 42.3769824, zoom = 14) %>% 
  addPolygons(data=el_osm, color='black', fillColor=colors, weight=1, dashArray=1, opacity=0.7, fillOpacity=0.5,
                    popup=unname(popups), 
                    options=c(pathOptions(), popupOptions(minWidth=100)))


# map = basemap('stamen.toner.lite')
# el_layer <- spLayer(el_osm, popup = unname(popups), stroke=TRUE, fill.col=colors)
# 
# writeMap(map, el_layer, width = 700, height = 500,
#          setView = c(42.3769824, -71.128184), setZoom = 14)      	

```
