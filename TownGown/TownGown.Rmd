---
title: "Cambridge Parcels owned by City, State and Universities"
author: "Kent Johnson"
date: '`r Sys.Date()`'
output:
  html_document:
    keep_md: yes
---

```{r parameters, echo=FALSE,include=FALSE,message=FALSE}
library(leaflet)
library(pander)
library(rgdal)
knitr::opts_chunk$set(echo=FALSE,fig.width=10, fig.height=8, 
                      comment=NA, warning=FALSE, message=FALSE)
options(width=100)
panderOptions("table.split.table", Inf)
```

```{r}
ass2015 = read.csv('../Cambridge Open Data/Assessing_Building_Information_clean_merged.csv', 
               stringsAsFactors=FALSE)

parcels2015 = readOGR('../Cambridge Open Data/ASSESSING_ParcelsFY2015.shp', 
                      'ASSESSING_ParcelsFY2015',
                      stringsAsFactors=FALSE, verbose=FALSE)

source('../Cambridge Open Data/OwnerNames.R')
source('../Cambridge Open Data/CODutil.R')

```


```{r}
# owner_names is a named list of all the spellings of one owner's name.
# We keep everything in parallel lists from here on; one list item per owner
owner_names = list(Harvard=harvard_names, MIT=mit_names, Lesley=lesley_names,
                   City=city_names, CHA=cha_names, CCH=cch_names, CAH=cah_names, 
                   Mass=mass_state_names, MBTA=mbta_names)
```
This document shows parcels in the assessor's database owned by

- City of Cambridge
- Cambridge Housing Authority (CHA)
- Cambridge Community Housing (CCH)
- Cambridge Affordable Housing (CAH)
- Commonwealth of Mass
- MBTA
- Harvard University
- MIT
- Lesley College / Lesley University

The document is based on data available from Cambridge GIS and the Cambridge Open Data portal at the time of writing:

- Assessors database from 
https://data.cambridgema.gov/Assessing/Assessing-Building-Information/crnm-mw9n
- Parcel map from 
http://www.cambridgema.gov/GIS/gisdatadictionary/Assessing/FY2015/ASSESSING_ParcelsFY2015.aspx
- Additional data from pre-release of Assessors open data

The map shows only parcels in the assessors database.

```{r}
# Assessed properties as lists of data frames, one for each owner
el = lapply(owner_names, function(ns) subset(ass2015, Owner.Name %in% ns))

# Get the actual parcels for each owner
el_parcels = lapply(el, function(elm) parcels2015[parcels2015$ML %in% elm$GIS.ID,])

owner_unit_counts = sapply(el, nrow)
owner_parcel_counts = sapply(el_parcels, length)
```

This map shows `r sum(owner_unit_counts)` assessed units (buildings, condos, etc.) in `r sum(owner_parcel_counts)` parcels with ownership as follows:

Units:
```{r}
pander(data.frame(as.list(owner_unit_counts)))
```

Parcels:
```{r}
pander(data.frame(as.list(owner_parcel_counts)))
```

This map shows the parcels.

```{r results='asis'}
palette = c(City='#006000', MIT='#A31F34', Harvard='#DC143C', Lesley='#804000',
            CHA='#80A000', CAH='#80A000', CCH='#80A000', Mass='#400080', MBTA='#A04060')
fill_palette = palette # Mostly the same
fill_palette['MIT']='#8A8B8C'
colorKey(palette)
```

```{r}
# Make a slippy map
# First the base layer
map = leaflet() %>% 
  addProviderTiles('Stamen.TonerLite') %>% 
  setView(-71.128184, 42.3769824, zoom = 14)

palette = c(City='#006000', MIT='#A31F34', Harvard='#DC143C', Lesley='#804000',
            CHA='#80A000', CAH='#80A000', CCH='#80A000', Mass='#400080', MBTA='#A04060')
fill_palette = palette # Mostly the same
fill_palette['MIT']='#8A8B8C'

# Add parcels for each owner to the map
for (owner in names(owner_names))
{
  # First show the parcels in el
  # There may be multiple units per parcel. 
  parcel_units = apply(el_parcels[[owner]]@data, 1, 
                       function(r) subset(el[[owner]], GIS.ID==r['ML']))
  
  # Make popups showing the map-lot, location and number of units.
  # Not every parcel has all data!
  popups = sapply(parcel_units, 
       function(units) {
         unit1 = as.list(units[1,])
         ml = unit1$GIS.ID
         paste0(
         '<p>Map-Lot: <a href="http://www.cambridgema.gov/propertydatabase/', 
         unit1$pid, '" target="_blank">' , ml, '</a><br>',
         unit1$Location, '<br>',
         'Owner: ', owner, '<br>',
         format_nblank(units$Building.Type, 'Building type: '),
         format_nblank(units$Style.Desc, 'Style: '),
         ifelse(owner=='City', format_nblank(unit1$Owner.s.Name2), ''),
         format_nblank(unit1$bldg_name),
         format_nblank(propclass(units$State.Use), 'Code: '), 
         format_nblank(unit1$use_type, 'Type: '),
         format_nblank(unit1$Year.Built, 'Year: '),
         format_nblank(sum(units$Living.Area, na.rm=TRUE), 'Living Area (sq ft): '),
         '</p>')
         })

  # Re-project into OSM projection
  el_osm = spTransform(el_parcels[[owner]], CRS("+init=EPSG:4326"))
  
  map = map %>% addPolygons(data=el_osm, 
                      color=palette[[owner]], fillColor=fill_palette[[owner]], 
                      opacity=0.7, fillOpacity=0.5,
                      popup=unname(popups), 
                      options=c(pathOptions(), popupOptions(minWidth=100)),
                      group=owner)

}

map = addLayersControl(map, overlayGroups = names(owner_names))
map
```
