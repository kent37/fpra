# Make the initial Affordable Housing reference

library(rgdal)
ass2015 = read.csv('../Cambridge Open Data/Assessing_Building_Information_clean_merged_w_zone.csv', 
               stringsAsFactors=FALSE)

realmast = read.csv('../Cambridge Open Data/realmast_clean.csv', stringsAsFactors=FALSE)
source('../Cambridge Open Data/OwnerNames.R')
source('../Cambridge Open Data/CODutil.R')

# Subset realmast to just items *not* in ass2015
realm = subset(realmast, !gis_id %in% ass2015$GIS.ID)

owner_names = list(CHA=cha_names, CCH=cch_names, CAH=cah_names, `121ACorp`=c(), Other=c())

# Assessed properties as lists of data frames, one for each owner
# We have one list for ass2015, one for realm
el = lapply(names(owner_names), function(n) 
{
  r = subset(ass2015, Owner.Name %in% owner_names[[n]])
  if (nrow(r) > 0)
  {
  r$Owner.Code=n
  r
  } else NULL
})
el$`121ACorp` = subset(ass2015, substr(State.Use, 0, 3)=='990')
el$`121ACorp`$Owner.Code = '121ACorp'
el$Other = subset(ass2015, GIS.ID %in% affordable_ml)
el$Other$Owner.Code = 'Other'

el2 = lapply(names(owner_names), function(n) 
{
  r = subset(realm, own_name %in% owner_names[[n]])
  if (nrow(r) > 0)
  {
  r$Owner.Code=n
  r
  } else NULL
})

el2$`121ACorp` = subset(realm, substr(use_code, 0, 3)=='990')
el2$`121ACorp`$Owner.Code = '121ACorp'

# Make a single dataset
el = do.call(rbind, el)
el = subset(el, select=c(GIS.ID, Location, Building.No, Building.Type, State.Use, Owner.Name, Owner.Code, Style.Desc, pid))

el2 = do.call(rbind, el2)
el2 = subset(el2, select=c(gis_id, prcl_locn, use_code, own_name, Owner.Code, pid))
names(el2) = c('GIS.ID', 'Location', 'State.Use', 'Owner.Name', 'Owner.Code', 'pid')
el2$Building.No = el2$Building.Type = el2$Style.Desc = NA

all = rbind(el, el2)
all$Property.Name = NA

# Reorder
all = all[,c("GIS.ID", "State.Use", "Owner.Code", "Owner.Name", "Location", "Property.Name", "Building.No", "Building.Type", "Style.Desc", "pid")]
write.csv(all, "AffordableHousingMaster.csv", row.names=FALSE)
