Sources:
- Assessors database from 
https://data.cambridgema.gov/Assessing/Assessing-Building-Information/crnm-mw9n

- Parcel map from 
http://www.cambridgema.gov/GIS/gisdatadictionary/Assessing/FY2015/ASSESSING_ParcelsFY2015.aspx
- Additional data from pre-release of Assessors open data and agency sites

- Cambridge CDD Active and Recent Development Projects
http://www.cambridgema.gov/CDD/housing/housingdevelopment/projects.aspx

- CHA Development Directory
http://cambridge-housing.org/iam/applicant/directory/default.asp

- Map of CPA Supported Affordable Housing Projects
http://www.cambridgema.gov/CPA/affordablehousing/mapofcpafundedaffordablehousingprojects.aspx

- CDD list of Other Housing Providers
http://www.cambridgema.gov/CDD/housing/resourcesandadditionalinformation/otherproviders.aspx

HUD Multifamily Portfolio Datasets
http://portal.hud.gov/hudportal/HUD?src=/program_offices/housing/mfh/presrv/mfhpreservation
Notes on HUD properties:
- The addresses in the HUD docs don't match exactly with Cambridge addresses.
  - 241 Garden vs 247 Garden
  - 401 Franklin vs 411 Franklin
  - 243 Broadway vs 240 Broadway
  - 362 Rindge Ave vs 400 Rindge - see http://www.schochet.com/Portfolio/tabid/94/agentType/View/PropertyID/11/Fresh-Pond-Apartments.aspx
  - 1 Citizen Place (Harwell Homes) - is this 40 Sciarappa St?
  - 1221 Cambridge vs 354 Prospect - CPA lists this as 116 affordable apts
  - 8 Marcella St is this 395 Cardinal Madeiros or 1-3 Marcella?
  - 14 Roosevelt vs 903 Cambridge
  - 21 Walden Sq vs 102 Sherman
  
80 Auburn Park - 77 units ML 92-130

- http://www.lowincomehousing.us/MA/cambridge.html
- http://affordablehousingonline.com/housing-search/Massachusetts/Cambridge/

LIHTC List:

To Do:

Brixton Arms is being purchased by Preservation of Affordable Housing (POAH) of Boston
See http://cambridge.wickedlocal.com/article/20150720/NEWS/150729753?utm_source=newsletter-20150721&utm_medium=email&utm_term=view_as_webpage&utm_campaign=newsletter

CHA Moving to Work
http://cambridge-housing.org/civicax/filebank/blobdload.aspx?blobid=22788
