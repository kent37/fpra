# Cambridge zones with 15% open space requirement

library(tidyverse)
library(sf)
library(mapview)
library(RColorBrewer)

zones = read_sf(here::here('Cambridge Open Data/Shapefiles/CDD_ZoningDistricts.shp/CDD_ZoningDistricts.shp'))

z15 = zones %>% 
  filter(ZONE_TYPE %in% c('BA-3', 'IB-1', 'A-1', 'A-2', 'B', 'C', 'C-1'))

camb = st_read(here::here('Cambridge Open Data/Shapefiles/BOUNDARY_CityBoundary.shp/BOUNDARY_CityBoundary.shp'),
               quiet=TRUE)

trees = st_read(here::here('Cambridge Open Data/Shapefiles/ENVIRONMENTAL_TreeCanopy2014.gdb.zip'))
                
mapview(z15, zcol='ZONE_TYPE', layer.name='Zone type',
        col.regions=brewer.pal(7, 'Paired'), alpha.regions=0.5) +
  mapview(canopy_per_block, zcol='coverage', col.regions=colorRampPalette(brewer.pal(9, 'YlGn')), layer.name='Percent canopy', lwd=0, alpha.regions=0.5) +
  mapview(camb, alpha.regions=0, legend=FALSE, lwd=2, color='gray50')
